package com.ifast.module.website.controller;

import com.ifast.module.website.config.WebsiteConfigProperties;
import com.ifast.common.utils.Result;
import com.ifast.common.utils.SpringContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author zhongzy on 2018-09-19.
 */
@Controller
@RequestMapping("/site")
public class IndexController {
    private String prefix = "boringsite/website";

    @GetMapping("")
    String toIndex(){
        WebsiteConfigProperties config = SpringContextHolder.getApplicationContext().getBean(WebsiteConfigProperties.class);
        return prefix + "/index_" + config.getTheme();
    }

    @GetMapping("/ok")
    public Result<String> save(){
        return Result.ok();
    }



}
