package com.ifast.module.website.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * 官网配置
 * </pre>
 * 
 * @author zhongzyong
 * @date 2018年9月19日 14:36:09
 *
 */
@Component
@ConfigurationProperties(prefix = "ifast.website")
@Data
public class WebsiteConfigProperties {
    /**
     * 站名
     */
    private String siteName;

    /**
     * 主题
     */
    private String theme;

}
