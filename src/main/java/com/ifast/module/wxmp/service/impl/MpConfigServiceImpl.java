package com.ifast.module.wxmp.service.impl;

import com.ifast.module.wxmp.dao.MpConfigDao;
import com.ifast.module.wxmp.domain.MpConfigDO;
import com.ifast.module.wxmp.service.MpConfigService;
import org.springframework.stereotype.Service;

import com.ifast.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 微信配置表
 * </pre>
 * <small> 2018-04-11 23:27:06 | Aron</small>
 */
@Service
public class MpConfigServiceImpl extends CoreServiceImpl<MpConfigDao, MpConfigDO> implements MpConfigService {

}
