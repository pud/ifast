package com.ifast.module.wxmp.service;

import com.ifast.module.wxmp.domain.MpConfigDO;
import com.ifast.common.base.CoreService;

/**
 * 
 * <pre>
 * 微信配置表
 * </pre>
 * <small> 2018-04-11 23:27:06 | Aron</small>
 */
public interface MpConfigService extends CoreService<MpConfigDO> {
    
}
