package com.ifast.module.wxmp.service.impl;

import com.ifast.module.wxmp.domain.MpFansDO;
import com.ifast.module.wxmp.service.MpFansService;
import org.springframework.stereotype.Service;

import com.ifast.module.wxmp.dao.MpFansDao;
import com.ifast.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 微信粉丝表
 * </pre>
 * <small> 2018-04-11 23:27:06 | Aron</small>
 */
@Service
public class MpFansServiceImpl extends CoreServiceImpl<MpFansDao, MpFansDO> implements MpFansService {

}
