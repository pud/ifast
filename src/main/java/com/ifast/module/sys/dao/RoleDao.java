package com.ifast.module.sys.dao;

import com.ifast.common.base.BaseDao;
import com.ifast.module.sys.domain.RoleDO;

/**
 * <pre>
 * 角色
 * </pre>
 * <small> 2018年3月23日 | Aron</small>
 */
public interface RoleDao extends BaseDao<RoleDO> {

}
