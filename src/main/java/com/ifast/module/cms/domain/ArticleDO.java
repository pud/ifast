package com.ifast.module.cms.domain;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.ifast.common.base.BaseDO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * <pre>
 * 文章表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("cms_article")
@EqualsAndHashCode(callSuper=true) 
public class ArticleDO extends BaseDO implements Serializable {
    private static final long serialVersionUID = 1L;
	@TableId
	private Long id;

    /** 栏目编号 */
    @TableField(value = "category_id", fill = FieldFill.INSERT_UPDATE)
    private String categoryId;

    /** 标题 */
    private String title;

    /** 文章链接 */
    private String link;

    /** 标题颜色 */
    private String color;

    /** 文章图片 */
    private String image;

    /** 关键字 */
    private String keywords;

    /** 描述、摘要 */
    private String description;

    /** 权重，越大越靠前 */
    private Integer weight;

    /** 权重期限 */
    @TableField(value = "weight_date", fill = FieldFill.INSERT_UPDATE)
    private Date weightDate;

    /** 点击数 */
    private Integer hits;

    /** 推荐位，多选 */
    private String posid;

    /** 自定义内容视图 */
    @TableField(value = "custom_content_view", fill = FieldFill.INSERT_UPDATE)
    private String customContentView;

    /** 视图配置 */
    @TableField(value = "view_config", fill = FieldFill.INSERT_UPDATE)
    private String viewConfig;

    /** 备注信息 */
    private String remarks;
}
