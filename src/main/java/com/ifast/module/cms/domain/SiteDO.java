package com.ifast.module.cms.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.ifast.common.base.BaseDO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * <pre>
 * 站点表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("cms_site")
@EqualsAndHashCode(callSuper=true) 
public class SiteDO extends BaseDO {
	@TableId
	private Long id;

    /** 站点名称 */
    private String name;

    /** 站点标题 */
    private String title;

    /** 站点Logo */
    private String logo;

    /** 站点域名 */
    private String domain;

    /** 描述 */
    private String description;

    /** 关键字 */
    private String keywords;

    /** 主题 */
    private String theme;

    /** 版权信息 */
    private String copyright;

    /** 自定义站点首页视图 */
    @TableField(value = "custom_index_view", fill = FieldFill.INSERT_UPDATE)
    private String customIndexView;

    /** 备注信息 */
    private String remarks;

}
