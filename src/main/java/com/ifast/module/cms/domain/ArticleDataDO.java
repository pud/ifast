package com.ifast.module.cms.domain;


import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;

import lombok.Data;

/**
 * 
 * <pre>
 * 文章详表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("cms_article_data")
//@EqualsAndHashCode(callSuper=true)
public class ArticleDataDO {
	@TableId
	private Long id;

    /** 文章内容 */
    private String content;

    /** 文章来源 */
    private String copyfrom;

    /** 相关文章 */
    private String relation;

    /** 是否允许评论 */
    @TableField(value = "allow_comment", fill = FieldFill.INSERT_UPDATE)
    private String allowComment;

}
