package com.ifast.module.cms.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;

import lombok.Data;

/**
 * 
 * <pre>
 * 留言板
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("cms_guestbook")
//@EqualsAndHashCode(callSuper=true)
public class GuestbookDO {
	@TableId
	private Long id;

    /** 留言分类 */
    private String type;

    /** 留言内容 */
    private String content;

    /** 姓名 */
    private String name;

    /** 邮箱 */
    private String email;

    /** 电话 */
    private String phone;

    /** 单位 */
    private String workunit;

    /** IP */
    private String ip;

    /** 留言时间 */
    @TableField(value = "create_date", fill = FieldFill.INSERT_UPDATE)
    private Date createDate;

    /** 回复人 */
    @TableField(value = "re_user_id", fill = FieldFill.INSERT_UPDATE)
    private Long reUserId;

    /** 回复时间 */
    @TableField(value = "re_date", fill = FieldFill.INSERT_UPDATE)
    private Date reDate;

    /** 回复内容 */
    @TableField(value = "re_content", fill = FieldFill.INSERT_UPDATE)
    private String reContent;

    /** 删除标记 */
    @TableField(value = "del_flag", fill = FieldFill.INSERT_UPDATE)
    private String delFlag;

}
