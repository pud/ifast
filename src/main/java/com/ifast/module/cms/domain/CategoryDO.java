package com.ifast.module.cms.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.ifast.common.base.BaseDO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * <pre>
 * 栏目表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("cms_category")
@EqualsAndHashCode(callSuper=true) 
public class CategoryDO extends BaseDO {
	@TableId
	private Long id;

    /** 父级编号 */
    @TableField(value = "parent_id", fill = FieldFill.INSERT_UPDATE)
    private String parentId;

    /** 所有父级编号 */
    @TableField(value = "parent_ids", fill = FieldFill.INSERT_UPDATE)
    private String parentIds;

    /** 站点编号 */
    @TableField(value = "site_id", fill = FieldFill.INSERT_UPDATE)
    private String siteId;

    /** 归属机构 */
    @TableField(value = "office_id", fill = FieldFill.INSERT_UPDATE)
    private String officeId;

    /** 栏目模块 */
    private String module;

    /** 栏目名称 */
    private String name;

    /** 栏目图片 */
    private String image;

    /** 链接 */
    private String href;

    /** 目标 */
    private String target;

    /** 描述 */
    private String description;

    /** 关键字 */
    private String keywords;

    /** 排序（升序） */
    private Integer sort;

    /** 是否在导航中显示 */
    @TableField(value = "in_menu", fill = FieldFill.INSERT_UPDATE)
    private String inMenu;

    /** 是否在分类页中显示列表 */
    @TableField(value = "in_list", fill = FieldFill.INSERT_UPDATE)
    private String inList;

    /** 展现方式 */
    @TableField(value = "show_modes", fill = FieldFill.INSERT_UPDATE)
    private String showModes;

    /** 是否允许评论 */
    @TableField(value = "allow_comment", fill = FieldFill.INSERT_UPDATE)
    private String allowComment;

    /** 是否需要审核 */
    @TableField(value = "is_audit", fill = FieldFill.INSERT_UPDATE)
    private String isAudit;

    /** 自定义列表视图 */
    @TableField(value = "custom_list_view", fill = FieldFill.INSERT_UPDATE)
    private String customListView;

    /** 自定义内容视图 */
    @TableField(value = "custom_content_view", fill = FieldFill.INSERT_UPDATE)
    private String customContentView;

    /** 视图配置 */
    @TableField(value = "view_config", fill = FieldFill.INSERT_UPDATE)
    private String viewConfig;
    /** 备注信息 */
    private String remarks;


}
