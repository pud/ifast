package com.ifast.module.cms.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.ifast.common.base.BaseDO;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * <pre>
 * 友情链接
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Data
@SuppressWarnings("serial")
@TableName("cms_link")
@EqualsAndHashCode(callSuper=true) 
public class LinkDO extends BaseDO {
	@TableId
	private Long id;

    /** 栏目编号 */
    @TableField(value = "category_id", fill = FieldFill.INSERT_UPDATE)
    private String categoryId;

    /** 链接名称 */
    private String title;

    /** 标题颜色 */
    private String color;

    /** 链接图片 */
    private String image;

    /** 链接地址 */
    private String href;

    /** 权重，越大越靠前 */
    private Integer weight;

    /** 权重期限 */
    @TableField(value = "weight_date", fill = FieldFill.INSERT_UPDATE)
    private Date weightDate;

    /** 备注信息 */
    private String remarks;

}
