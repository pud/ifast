package com.ifast.module.cms.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.FieldFill;

import lombok.Data;

/**
 * 
 * <pre>
 * 评论表
 * </pre>
 * <small> 2018-09-06 16:55:47 |</small>
 * @author kanchai
 */
@Data
@SuppressWarnings("serial")
@TableName("cms_comment")
//@EqualsAndHashCode(callSuper=true)
public class CommentDO {
	@TableId
	private Long id;

    /** 栏目编号 */
    @TableField(value = "category_id", fill = FieldFill.INSERT_UPDATE)
    private String categoryId;

    /** 栏目内容的编号 */
    @TableField(value = "content_id", fill = FieldFill.INSERT_UPDATE)
    private String contentId;

    /** 栏目内容的标题 */
    private String title;

    /** 评论内容 */
    private String content;

    /** 评论姓名 */
    private String name;

    /** 评论IP */
    private String ip;

    /** 评论时间 */
    @TableField(value = "create_date", fill = FieldFill.INSERT_UPDATE)
    private Date createDate;

    /** 审核人 */
    @TableField(value = "audit_user_id", fill = FieldFill.INSERT_UPDATE)
    private String auditUserId;

    /** 审核时间 */
    @TableField(value = "audit_date", fill = FieldFill.INSERT_UPDATE)
    private Date auditDate;

    /** 删除标记 */
    @TableField(value = "delf_lag", fill = FieldFill.INSERT_UPDATE)
    private String delFlag;

}
