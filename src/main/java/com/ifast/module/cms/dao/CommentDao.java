package com.ifast.module.cms.dao;

import com.ifast.module.cms.domain.CommentDO;
import com.ifast.common.base.BaseDao;

/**
 * 
 * <pre>
 * 评论表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface CommentDao extends BaseDao<CommentDO> {

}
