package com.ifast.module.cms.dao;

import com.ifast.module.cms.domain.GuestbookDO;
import com.ifast.common.base.BaseDao;

/**
 * 
 * <pre>
 * 留言板
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface GuestbookDao extends BaseDao<GuestbookDO> {

}
