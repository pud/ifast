package com.ifast.module.cms.dao;

import com.ifast.module.cms.domain.ArticleDataDO;
import com.ifast.common.base.BaseDao;

/**
 * 
 * <pre>
 * 文章详表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface ArticleDataDao extends BaseDao<ArticleDataDO> {

}
