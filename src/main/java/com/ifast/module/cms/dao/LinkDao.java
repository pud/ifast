package com.ifast.module.cms.dao;

import com.ifast.module.cms.domain.LinkDO;
import com.ifast.common.base.BaseDao;

/**
 * 
 * <pre>
 * 友情链接
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface LinkDao extends BaseDao<LinkDO> {

}
