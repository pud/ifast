package com.ifast.module.cms.dao;

import com.ifast.module.cms.domain.SiteDO;
import com.ifast.common.base.BaseDao;

/**
 * 
 * <pre>
 * 站点表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface SiteDao extends BaseDao<SiteDO> {

}
