package com.ifast.module.cms.dao;

import com.ifast.module.cms.domain.ArticleDO;
import com.ifast.common.base.BaseDao;

/**
 * 
 * <pre>
 * 文章表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 * @author  kanchai
 */
public interface ArticleDao extends BaseDao<ArticleDO> {

}
