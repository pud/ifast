package com.ifast.module.cms.dao;

import com.ifast.module.cms.domain.CategoryDO;
import com.ifast.common.base.BaseDao;

/**
 * 
 * <pre>
 * 栏目表
 * </pre>
 * <small> 2018-09-06 16:55:47 </small>
 * @author kanchai
 */
public interface CategoryDao extends BaseDao<CategoryDO> {

}
