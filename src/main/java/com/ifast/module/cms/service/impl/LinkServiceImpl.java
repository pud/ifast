package com.ifast.module.cms.service.impl;

import com.ifast.module.cms.dao.LinkDao;
import com.ifast.module.cms.domain.LinkDO;
import com.ifast.module.cms.service.LinkService;
import com.ifast.common.base.CoreServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 
 * <pre>
 * 友情链接
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Service
public class LinkServiceImpl extends CoreServiceImpl<LinkDao, LinkDO> implements LinkService {

}
