package com.ifast.module.cms.service.impl;

import com.ifast.module.cms.dao.SiteDao;
import com.ifast.module.cms.domain.SiteDO;
import com.ifast.module.cms.service.SiteService;
import org.springframework.stereotype.Service;

import com.ifast.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 站点表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Service
public class SiteServiceImpl extends CoreServiceImpl<SiteDao, SiteDO> implements SiteService {

}
