package com.ifast.module.cms.service;

import com.ifast.module.cms.domain.CommentDO;
import com.ifast.common.base.CoreService;

/**
 * 
 * <pre>
 * 评论表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface CommentService extends CoreService<CommentDO> {
    
}
