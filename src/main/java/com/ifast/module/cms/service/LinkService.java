package com.ifast.module.cms.service;

import com.ifast.common.base.CoreService;
import com.ifast.module.cms.domain.LinkDO;

/**
 * 
 * <pre>
 * 友情链接
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface LinkService extends CoreService<LinkDO> {
    
}
