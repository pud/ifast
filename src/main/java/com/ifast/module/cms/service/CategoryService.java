package com.ifast.module.cms.service;

import com.ifast.module.cms.domain.CategoryDO;
import com.ifast.common.base.CoreService;

/**
 * 
 * <pre>
 * 栏目表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface CategoryService extends CoreService<CategoryDO> {
    
}
