package com.ifast.module.cms.service.impl;

import com.ifast.module.cms.dao.ArticleDao;
import com.ifast.module.cms.domain.ArticleDO;
import com.ifast.module.cms.service.ArticleService;
import com.ifast.common.base.CoreServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 
 * <pre>
 * 文章表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Service
public class ArticleServiceImpl extends CoreServiceImpl<ArticleDao, ArticleDO> implements ArticleService {

}
