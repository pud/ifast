package com.ifast.module.cms.service.impl;

import com.ifast.module.cms.dao.GuestbookDao;
import com.ifast.module.cms.domain.GuestbookDO;
import com.ifast.module.cms.service.GuestbookService;
import org.springframework.stereotype.Service;

import com.ifast.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 留言板
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Service
public class GuestbookServiceImpl extends CoreServiceImpl<GuestbookDao, GuestbookDO> implements GuestbookService {

}
