package com.ifast.module.cms.service.impl;

import com.ifast.module.cms.dao.ArticleDataDao;
import com.ifast.module.cms.domain.ArticleDataDO;
import com.ifast.module.cms.service.ArticleDataService;
import org.springframework.stereotype.Service;

import com.ifast.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 文章详表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Service
public class ArticleDataServiceImpl extends CoreServiceImpl<ArticleDataDao, ArticleDataDO> implements ArticleDataService {

}
