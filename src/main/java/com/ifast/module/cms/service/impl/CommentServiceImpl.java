package com.ifast.module.cms.service.impl;

import com.ifast.module.cms.dao.CommentDao;
import com.ifast.module.cms.domain.CommentDO;
import com.ifast.module.cms.service.CommentService;
import org.springframework.stereotype.Service;

import com.ifast.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 评论表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Service
public class CommentServiceImpl extends CoreServiceImpl<CommentDao, CommentDO> implements CommentService {

}
