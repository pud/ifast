package com.ifast.module.cms.service;

import com.ifast.module.cms.domain.ArticleDO;
import com.ifast.common.base.CoreService;

/**
 * 
 * <pre>
 * 文章表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface ArticleService extends CoreService<ArticleDO> {
    
}
