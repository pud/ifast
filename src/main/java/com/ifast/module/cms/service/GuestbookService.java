package com.ifast.module.cms.service;

import com.ifast.module.cms.domain.GuestbookDO;
import com.ifast.common.base.CoreService;

/**
 * 
 * <pre>
 * 留言板
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface GuestbookService extends CoreService<GuestbookDO> {
    
}
