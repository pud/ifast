package com.ifast.module.cms.service.impl;

import com.ifast.module.cms.dao.CategoryDao;
import com.ifast.module.cms.domain.CategoryDO;
import com.ifast.module.cms.service.CategoryService;
import org.springframework.stereotype.Service;

import com.ifast.common.base.CoreServiceImpl;

/**
 * 
 * <pre>
 * 栏目表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Service
public class CategoryServiceImpl extends CoreServiceImpl<CategoryDao, CategoryDO> implements CategoryService {

}
