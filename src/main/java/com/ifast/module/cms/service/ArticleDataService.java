package com.ifast.module.cms.service;

import com.ifast.module.cms.domain.ArticleDataDO;
import com.ifast.common.base.CoreService;

/**
 * 
 * <pre>
 * 文章详表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
public interface ArticleDataService extends CoreService<ArticleDataDO> {
    
}
