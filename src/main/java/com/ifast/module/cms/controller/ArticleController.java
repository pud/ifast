package com.ifast.module.cms.controller;


import java.util.Arrays;

import com.ifast.module.cms.domain.ArticleDO;
import com.ifast.module.cms.service.ArticleService;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

/**
 * 
 * <pre>
 * 文章表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Controller
@RequestMapping("/cms/article")
@Api(value="/cms/article", description="文章管理", produces = "/cms/article")
public class ArticleController extends AdminBaseController {
	@Autowired
	private ArticleService articleService;
	
	@GetMapping()
	@RequiresPermissions("ifast:article:article")
	String Article(){
	    return "boringsite/cms/article/article";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("ifast:article:article")
	public Result<Page<ArticleDO>> list(ArticleDO articleDTO){
        Wrapper<ArticleDO> wrapper = new EntityWrapper<ArticleDO>().orderBy("id", false);
        Page<ArticleDO> page = new Page<>();
		try {
			page = articleService.selectPage(getPage(ArticleDO.class), wrapper);
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("ifast:article:add")
	String add(){
	    return "boringsite/cms/article/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("ifast:article:edit")
	String edit(@PathVariable("id") String id,Model model){
		ArticleDO article = articleService.selectById(id);
		model.addAttribute("article", article);
	    return "boringsite/cms/article/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("ifast:article:add")
	public Result<String> save(ArticleDO article){
		articleService.insert(article);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("ifast:article:edit")
	public Result<String>  update(ArticleDO article){
		boolean update = articleService.updateById(article);
		return update ? Result.ok() : Result.fail();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("ifast:article:remove")
	public Result<String>  remove(String id){
		articleService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("ifast:article:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") String[] ids){
		articleService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
