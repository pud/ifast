package com.ifast.module.cms.controller;


import java.util.Arrays;

import com.ifast.module.cms.domain.SiteDO;
import com.ifast.module.cms.service.SiteService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

/**
 * 
 * <pre>
 * 站点表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Controller
@RequestMapping("/cms/site")
public class SiteController extends AdminBaseController {
	@Autowired
	private SiteService siteService;
	
	@GetMapping()
	@RequiresPermissions("ifast:site:site")
	String Site(){
	    return "boringsite/cms/site/site";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("ifast:site:site")
	public Result<Page<SiteDO>> list(SiteDO siteDTO){
        Wrapper<SiteDO> wrapper = new EntityWrapper<SiteDO>().orderBy("id", false);
        Page<SiteDO> page = siteService.selectPage(getPage(SiteDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("ifast:site:add")
	String add(){
	    return "boringsite/cms/site/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("ifast:site:edit")
	String edit(@PathVariable("id") String id,Model model){
		SiteDO site = siteService.selectById(id);
		model.addAttribute("site", site);
	    return "boringsite/cms/site/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("ifast:site:add")
	public Result<String> save( SiteDO site){
		siteService.insert(site);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("ifast:site:edit")
	public Result<String>  update( SiteDO site){
		boolean update = siteService.updateById(site);
		return update ? Result.ok() : Result.fail();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("ifast:site:remove")
	public Result<String>  remove( String id){
		siteService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("ifast:site:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") String[] ids){
		siteService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
