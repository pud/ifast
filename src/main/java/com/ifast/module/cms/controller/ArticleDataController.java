package com.ifast.module.cms.controller;


import java.util.Arrays;

import com.ifast.module.cms.domain.ArticleDataDO;
import com.ifast.module.cms.service.ArticleDataService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

/**
 * 
 * <pre>
 * 文章详表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Controller
@RequestMapping("/cms/articleData")
public class ArticleDataController extends AdminBaseController {
	@Autowired
	private ArticleDataService articleDataService;
	
	@GetMapping()
	@RequiresPermissions("ifast:articleData:articleData")
	String ArticleData(){
	    return "boringsite/cms/articleData/articleData";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("ifast:articleData:articleData")
	public Result<Page<ArticleDataDO>> list(ArticleDataDO articleDataDTO){
        Wrapper<ArticleDataDO> wrapper = new EntityWrapper<ArticleDataDO>().orderBy("id", false);
        Page<ArticleDataDO> page = articleDataService.selectPage(getPage(ArticleDataDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("ifast:articleData:add")
	String add(){
	    return "boringsite/cms/articleData/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("ifast:articleData:edit")
	String edit(@PathVariable("id") String id,Model model){
		ArticleDataDO articleData = articleDataService.selectById(id);
		model.addAttribute("articleData", articleData);
	    return "boringsite/cms/articleData/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("ifast:articleData:add")
	public Result<String> save( ArticleDataDO articleData){
		articleDataService.insert(articleData);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("ifast:articleData:edit")
	public Result<String>  update( ArticleDataDO articleData){
		boolean update = articleDataService.updateById(articleData);
		return update ? Result.ok() : Result.fail();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("ifast:articleData:remove")
	public Result<String>  remove( String id){
		articleDataService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("ifast:articleData:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") String[] ids){
		articleDataService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
