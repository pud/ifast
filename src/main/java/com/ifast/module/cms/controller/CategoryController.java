package com.ifast.module.cms.controller;


import java.util.Arrays;

import com.ifast.module.cms.domain.CategoryDO;
import com.ifast.module.cms.service.CategoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

/**
 * 
 * <pre>
 * 栏目表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Controller
@RequestMapping("/cms/category")
public class CategoryController extends AdminBaseController {
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping()
	@RequiresPermissions("ifast:category:category")
	String Category(){
	    return "boringsite/cms/category/category";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("ifast:category:category")
	public Result<Page<CategoryDO>> list(CategoryDO categoryDTO){
        Wrapper<CategoryDO> wrapper = new EntityWrapper<CategoryDO>().orderBy("id", false);
        Page<CategoryDO> page = categoryService.selectPage(getPage(CategoryDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("ifast:category:add")
	String add(){
	    return "boringsite/cms/category/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("ifast:category:edit")
	String edit(@PathVariable("id") String id,Model model){
		CategoryDO category = categoryService.selectById(id);
		model.addAttribute("category", category);
	    return "boringsite/cms/category/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("ifast:category:add")
	public Result<String> save( CategoryDO category){
		categoryService.insert(category);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("ifast:category:edit")
	public Result<String>  update( CategoryDO category){
		boolean update = categoryService.updateById(category);
		return update ? Result.ok() : Result.fail();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("ifast:category:remove")
	public Result<String>  remove( String id){
		categoryService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("ifast:category:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") String[] ids){
		categoryService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
