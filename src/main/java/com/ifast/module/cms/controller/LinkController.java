package com.ifast.module.cms.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;
import com.ifast.module.cms.domain.LinkDO;
import com.ifast.module.cms.service.LinkService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * 
 * <pre>
 * 友情链接
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Controller
@RequestMapping("/cms/link")
public class LinkController extends AdminBaseController {
	@Autowired
	private LinkService linkService;
	
	@GetMapping()
	@RequiresPermissions("ifast:link:link")
	String Link(){
	    return "boringsite/cms/link/link";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("ifast:link:link")
	public Result<Page<LinkDO>> list(LinkDO linkDTO){
        Wrapper<LinkDO> wrapper = new EntityWrapper<LinkDO>().orderBy("id", false);
        Page<LinkDO> page = linkService.selectPage(getPage(LinkDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("ifast:link:add")
	String add(){
	    return "boringsite/cms/link/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("ifast:link:edit")
	String edit(@PathVariable("id") String id,Model model){
		LinkDO link = linkService.selectById(id);
		model.addAttribute("link", link);
	    return "boringsite/cms/link/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("ifast:link:add")
	public Result<String> save( LinkDO link){
		linkService.insert(link);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("ifast:link:edit")
	public Result<String>  update( LinkDO link){
		boolean update = linkService.updateById(link);
		return update ? Result.ok() : Result.fail();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("ifast:link:remove")
	public Result<String>  remove( String id){
		linkService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("ifast:link:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") String[] ids){
		linkService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
