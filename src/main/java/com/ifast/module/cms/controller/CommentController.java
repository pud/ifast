package com.ifast.module.cms.controller;


import java.util.Arrays;

import com.ifast.module.cms.domain.CommentDO;
import com.ifast.module.cms.service.CommentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

/**
 * 
 * <pre>
 * 评论表
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Controller
@RequestMapping("/cms/comment")
public class CommentController extends AdminBaseController {
	@Autowired
	private CommentService commentService;
	
	@GetMapping()
	@RequiresPermissions("ifast:comment:comment")
	String Comment(){
	    return "boringsite/cms/comment/comment";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("ifast:comment:comment")
	public Result<Page<CommentDO>> list(CommentDO commentDTO){
        Wrapper<CommentDO> wrapper = new EntityWrapper<CommentDO>().orderBy("id", false);
        Page<CommentDO> page = commentService.selectPage(getPage(CommentDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("ifast:comment:add")
	String add(){
	    return "boringsite/cms/comment/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("ifast:comment:edit")
	String edit(@PathVariable("id") String id,Model model){
		CommentDO comment = commentService.selectById(id);
		model.addAttribute("comment", comment);
	    return "boringsite/cms/comment/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("ifast:comment:add")
	public Result<String> save( CommentDO comment){
		commentService.insert(comment);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("ifast:comment:edit")
	public Result<String>  update( CommentDO comment){
		boolean update = commentService.updateById(comment);
		return update ? Result.ok() : Result.fail();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("ifast:comment:remove")
	public Result<String>  remove( String id){
		commentService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("ifast:comment:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") String[] ids){
		commentService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
