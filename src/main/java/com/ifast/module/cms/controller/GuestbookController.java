package com.ifast.module.cms.controller;


import java.util.Arrays;

import com.ifast.module.cms.domain.GuestbookDO;
import com.ifast.module.cms.service.GuestbookService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;

/**
 * 
 * <pre>
 * 留言板
 * </pre>
 * <small> 2018-09-06 16:55:47 | Aron</small>
 */
@Controller
@RequestMapping("/cms/guestbook")
public class GuestbookController extends AdminBaseController {
	@Autowired
	private GuestbookService guestbookService;
	
	@GetMapping()
	@RequiresPermissions("ifast:guestbook:guestbook")
	String Guestbook(){
	    return "boringsite/cms/guestbook/guestbook";
	}
	
	@ResponseBody
	@GetMapping("/list")
	@RequiresPermissions("ifast:guestbook:guestbook")
	public Result<Page<GuestbookDO>> list(GuestbookDO guestbookDTO){
        Wrapper<GuestbookDO> wrapper = new EntityWrapper<GuestbookDO>().orderBy("id", false);
        Page<GuestbookDO> page = guestbookService.selectPage(getPage(GuestbookDO.class), wrapper);
        return Result.ok(page);
	}
	
	@GetMapping("/add")
	@RequiresPermissions("ifast:guestbook:add")
	String add(){
	    return "boringsite/cms/guestbook/add";
	}

	@GetMapping("/edit/{id}")
	@RequiresPermissions("ifast:guestbook:edit")
	String edit(@PathVariable("id") String id,Model model){
		GuestbookDO guestbook = guestbookService.selectById(id);
		model.addAttribute("guestbook", guestbook);
	    return "boringsite/cms/guestbook/edit";
	}
	
	/**
	 * 保存
	 */
	@ResponseBody
	@PostMapping("/save")
	@RequiresPermissions("ifast:guestbook:add")
	public Result<String> save( GuestbookDO guestbook){
		guestbookService.insert(guestbook);
        return Result.ok();
	}
	/**
	 * 修改
	 */
	@ResponseBody
	@RequestMapping("/update")
	@RequiresPermissions("ifast:guestbook:edit")
	public Result<String>  update( GuestbookDO guestbook){
		boolean update = guestbookService.updateById(guestbook);
		return update ? Result.ok() : Result.fail();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/remove")
	@ResponseBody
	@RequiresPermissions("ifast:guestbook:remove")
	public Result<String>  remove( String id){
		guestbookService.deleteById(id);
        return Result.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping( "/batchRemove")
	@ResponseBody
	@RequiresPermissions("ifast:guestbook:batchRemove")
	public Result<String>  remove(@RequestParam("ids[]") String[] ids){
		guestbookService.deleteBatchIds(Arrays.asList(ids));
		return Result.ok();
	}
	
}
