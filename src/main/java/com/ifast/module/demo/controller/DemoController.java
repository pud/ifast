package com.ifast.module.demo.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.utils.Result;
import com.ifast.module.demo.domain.DemoDO;
import com.ifast.module.demo.service.DemoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * <pre>
 * 基础表
 * </pre>
 * <small> 2018-07-27 23:38:24 | Aron</small>
 */
@Controller
@RequestMapping("/demo/demoBase")
@Api(value="/demo/demoBase", description="代码示例")
public class DemoController extends AdminBaseController {
    @Autowired
    private DemoService demoBaseService;

    @GetMapping()
    @RequiresPermissions("demo:demoBase:demoBase")
    @ApiOperation(value="进入demo页面", notes = "说明")
    String DemoBase() {
        return "demo/demoBase/demoBase";
    }

    @ResponseBody
    @GetMapping("/list")
    @RequiresPermissions("demo:demoBase:demoBase")
    @ApiOperation(value="查看所有数据", produces = "text/plain;charset=UTF8", response=DemoDO.class)
    public Result<Page<DemoDO>> list(DemoDO demoBaseDTO) {
        Wrapper<DemoDO> wrapper = new EntityWrapper<DemoDO>().orderBy("id", false);
        wrapper.like("title", demoBaseDTO.getTitle());
        wrapper.like("content", demoBaseDTO.getContent());
        Page<DemoDO> page = demoBaseService.selectPage(getPage(DemoDO.class), wrapper);
        return Result.ok(page);
    }

    @GetMapping("/add")
    @RequiresPermissions("demo:demoBase:add")
    String add() {
        return "demo/demoBase/add";
    }

    @GetMapping("/edit/{id}")
    @RequiresPermissions("demo:demoBase:edit")
    String edit(@PathVariable("id") Long id, Model model) {
        DemoDO demoBase = demoBaseService.selectById(id);
        model.addAttribute("demoBase", demoBase);
        return "demo/demoBase/edit";
    }

    /**
     * 保存
     */
    @ResponseBody
    @PostMapping("/save")
    @RequiresPermissions("demo:demoBase:add")
    @ApiOperation(value="新增数据", produces = "text/plain;charset=UTF8")
    public Result<String> save(DemoDO demoBase) {
        boolean insert = demoBaseService.insert(demoBase);
        return insert ? Result.ok() : Result.fail();
    }

    /**
     * 修改
     */
    @ResponseBody
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @RequiresPermissions("demo:demoBase:edit")
    @ApiOperation(value="更新数据", produces = "text/plain;charset=UTF8")
    public Result<String> update(DemoDO demoBase) {
        boolean updateById = demoBaseService.updateById(demoBase);
        return updateById ? Result.ok() : Result.fail();
    }

    /**
     * 删除
     */
    @RequestMapping(value = "/remove", method=RequestMethod.DELETE)
    @ResponseBody
    @RequiresPermissions("demo:demoBase:remove")
    @ApiOperation(value="删除", produces = "text/plain;charset=UTF8")
    public Result<String> remove(Long id) {
        demoBaseService.deleteById(id);
        return Result.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/batchRemove")
    @ResponseBody
    @RequiresPermissions("demo:demoBase:batchRemove")
    @ApiOperation(value="批量删除", produces = "text/plain;charset=UTF8")
    public Result<String> remove(@RequestParam("ids[]") Long[] ids) {
        demoBaseService.deleteBatchIds(Arrays.asList(ids));
        return Result.ok();
    }

}
