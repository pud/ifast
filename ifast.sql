/*
Navicat MySQL Data Transfer

Source Server         : 本机
Source Server Version : 50173
Source Host           : localhost:3306
Source Database       : ifast

Target Server Type    : MYSQL
Target Server Version : 50173
File Encoding         : 65001

Date: 2018-09-07 16:45:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for app_demo_base
-- ----------------------------
DROP TABLE IF EXISTS `app_demo_base`;
CREATE TABLE `app_demo_base` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `publish` datetime DEFAULT NULL COMMENT '发布时间',
  `content` mediumtext COMMENT '正文',
  `deleted` bit(1) DEFAULT b'0' COMMENT '删除',
  `version` smallint(6) DEFAULT '0' COMMENT '版本',
  `createAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `updateAt` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `createBy` bigint(20) DEFAULT NULL COMMENT '创建者',
  `updateBy` bigint(20) DEFAULT NULL COMMENT '更新者',
  `price` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='基础表';

-- ----------------------------
-- Records of app_demo_base
-- ----------------------------
INSERT INTO `app_demo_base` VALUES ('1', '测试文档', '2018-08-17 11:59:00', '测试文档', '\0', '8', '2018-09-05 16:12:01', '2018-08-22 15:34:46', '1', '1', null);

-- ----------------------------
-- Table structure for cms_article
-- ----------------------------
DROP TABLE IF EXISTS `cms_article`;
CREATE TABLE `cms_article` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `category_id` bigint(64) NOT NULL COMMENT '栏目编号',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `link` varchar(255) DEFAULT NULL COMMENT '文章链接',
  `color` varchar(50) DEFAULT NULL COMMENT '标题颜色',
  `image` varchar(255) DEFAULT NULL COMMENT '文章图片',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `description` varchar(255) DEFAULT NULL COMMENT '描述、摘要',
  `weight` int(11) DEFAULT '0' COMMENT '权重，越大越靠前',
  `weight_date` datetime DEFAULT NULL COMMENT '权重期限',
  `hits` int(11) DEFAULT '0' COMMENT '点击数',
  `posid` varchar(10) DEFAULT NULL COMMENT '推荐位，多选',
  `custom_content_view` varchar(255) DEFAULT NULL COMMENT '自定义内容视图',
  `view_config` text COMMENT '视图配置',
  `createBy` bigint(20) DEFAULT NULL COMMENT '创建者',
  `createAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updateBy` bigint(20) DEFAULT NULL COMMENT '更新者',
  `updateAt` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `deleted` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_article_create_by` (`createBy`) USING BTREE,
  KEY `cms_article_title` (`title`) USING BTREE,
  KEY `cms_article_keywords` (`keywords`) USING BTREE,
  KEY `cms_article_del_flag` (`deleted`) USING BTREE,
  KEY `cms_article_weight` (`weight`) USING BTREE,
  KEY `cms_article_update_date` (`updateAt`) USING BTREE,
  KEY `cms_article_category_id` (`category_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of cms_article
-- ----------------------------
INSERT INTO `cms_article` VALUES ('1', '3', '文章标题标题标题标题', null, 'green', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('2', '3', '文章标题标题标题标题', null, 'red', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('3', '3', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('4', '3', '文章标题标题标题标题', null, 'green', '', '关键字1,关键字2', '', '0', null, '1', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('5', '3', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('6', '3', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('7', '4', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('8', '4', '文章标题标题标题标题', null, 'blue', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('9', '4', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('10', '4', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('11', '5', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('12', '5', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_article` VALUES ('13', '5', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('14', '7', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('15', '7', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('16', '7', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('17', '7', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('18', '8', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('19', '8', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('20', '8', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('21', '8', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('22', '9', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('23', '9', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('24', '9', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('25', '9', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('26', '9', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('27', '11', '葫芦娃强势插入地税行业', '', '', '', '葫芦娃', '', '0', null, '0', ',null,', '', '', '1', '2013-05-27 00:00:00', '1', '2015-03-13 15:52:32', '', '1', null);
INSERT INTO `cms_article` VALUES ('28', '11', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('29', '11', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('30', '11', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('31', '11', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('32', '12', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('33', '12', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('34', '12', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('35', '12', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('36', '12', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('37', '13', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('38', '13', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('39', '13', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('40', '13', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('41', '14', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('42', '14', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('43', '14', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('44', '14', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('45', '14', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('46', '15', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('47', '15', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('48', '15', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('49', '16', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('50', '17', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('51', '17', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('52', '26', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_article` VALUES ('53', '26', '文章标题标题标题标题', null, '', '', '关键字1,关键字2', '', '0', null, '0', '', null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);

-- ----------------------------
-- Table structure for cms_article_data
-- ----------------------------
DROP TABLE IF EXISTS `cms_article_data`;
CREATE TABLE `cms_article_data` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `content` text COMMENT '文章内容',
  `copyfrom` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `relation` varchar(255) DEFAULT NULL COMMENT '相关文章',
  `allow_comment` char(1) DEFAULT NULL COMMENT '是否允许评论',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章详表';

-- ----------------------------
-- Records of cms_article_data
-- ----------------------------
INSERT INTO `cms_article_data` VALUES ('1', '文章内容内容内容内容', '来源', '1,2,3', '1');
INSERT INTO `cms_article_data` VALUES ('10', '文章内容内容内容内容', '来源', '1,2,3', '1');
INSERT INTO `cms_article_data` VALUES ('11', '文章内容内容内容内容', '来源', '1,2,3', '1');

-- ----------------------------
-- Table structure for cms_category
-- ----------------------------
DROP TABLE IF EXISTS `cms_category`;
CREATE TABLE `cms_category` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `parent_id` varchar(64) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(2000) NOT NULL COMMENT '所有父级编号',
  `site_id` varchar(64) DEFAULT '1' COMMENT '站点编号',
  `office_id` varchar(64) DEFAULT NULL COMMENT '归属机构',
  `module` varchar(20) DEFAULT NULL COMMENT '栏目模块',
  `name` varchar(100) NOT NULL COMMENT '栏目名称',
  `image` varchar(255) DEFAULT NULL COMMENT '栏目图片',
  `href` varchar(255) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) DEFAULT NULL COMMENT '目标',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `sort` int(11) DEFAULT '30' COMMENT '排序（升序）',
  `in_menu` char(1) DEFAULT '1' COMMENT '是否在导航中显示',
  `in_list` char(1) DEFAULT '1' COMMENT '是否在分类页中显示列表',
  `show_modes` char(1) DEFAULT '0' COMMENT '展现方式',
  `allow_comment` char(1) DEFAULT NULL COMMENT '是否允许评论',
  `is_audit` char(1) DEFAULT NULL COMMENT '是否需要审核',
  `custom_list_view` varchar(255) DEFAULT NULL COMMENT '自定义列表视图',
  `custom_content_view` varchar(255) DEFAULT NULL COMMENT '自定义内容视图',
  `view_config` text COMMENT '视图配置',
  `createBy` bigint(20) DEFAULT NULL COMMENT '创建者',
  `createAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updateBy` bigint(20) DEFAULT NULL COMMENT '更新者',
  `updateAt` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `deleted` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_category_parent_id` (`parent_id`) USING BTREE,
  KEY `cms_category_parent_ids` (`parent_ids`(255)) USING BTREE,
  KEY `cms_category_module` (`module`) USING BTREE,
  KEY `cms_category_name` (`name`) USING BTREE,
  KEY `cms_category_sort` (`sort`) USING BTREE,
  KEY `cms_category_del_flag` (`deleted`) USING BTREE,
  KEY `cms_category_office_id` (`office_id`) USING BTREE,
  KEY `cms_category_site_id` (`site_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='栏目表';

-- ----------------------------
-- Records of cms_category
-- ----------------------------
INSERT INTO `cms_category` VALUES ('1', '0', '0,', '0', '1', '', '顶级栏目', '', '', '', '', '', '0', '1', '1', '0', '0', '1', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_category` VALUES ('10', '1', '0,1,', '1', '4', 'article', '软件介绍', '', '', '', '', '', '20', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_category` VALUES ('11', '10', '0,1,10,', '1', '4', 'article', '网络工具', '', '', '', '', '', '30', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_category` VALUES ('12', '10', '0,1,10,', '1', '4', 'article', '浏览工具', '', '', '', '', '', '40', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_category` VALUES ('13', '10', '0,1,10,', '1', '4', 'article', '浏览辅助', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_category` VALUES ('14', '10', '0,1,10,', '1', '4', 'article', '网络优化', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_category` VALUES ('15', '10', '0,1,10,', '1', '4', 'article', '邮件处理', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_category` VALUES ('16', '10', '0,1,10,', '1', '4', 'article', '下载工具', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('17', '10', '0,1,10,', '1', '4', 'article', '搜索工具', '', '', '', '', '', '50', '1', '1', '2', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('18', '1', '0,1,', '1', '5', 'link', '友情链接', '', '', '', '', '', '90', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('19', '18', '0,1,18,', '1', '5', 'link', '常用网站', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('2', '1', '0,1,', '1', '3', 'article', '组织机构', '', '', '', '', '', '10', '1', '1', '0', '0', '1', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('20', '18', '0,1,18,', '1', '5', 'link', '门户网站', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('21', '18', '0,1,18,', '1', '5', 'link', '购物网站', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('22', '18', '0,1,18,', '1', '5', 'link', '交友社区', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('23', '18', '0,1,18,', '1', '5', 'link', '音乐视频', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('24', '1', '0,1,', '1', '6', '', '百度一下', '', 'http://www.baidu.com', '_blank', '', '', '90', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('25', '1', '0,1,', '1', '6', '', '全文检索', '', '/search', '', '', '', '90', '0', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('26', '1', '0,1,', '2', '6', 'article', '测试栏目', '', '', '', '', '', '90', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('27', '1', '0,1,', '1', '6', '', '公共留言', '', '/guestbook', '', '', '', '90', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('3', '2', '0,1,2,', '1', '3', 'article', '网站简介', '', '', '', '', '', '30', '1', '1', '0', '0', '1', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('4', '2', '0,1,2,', '1', '3', 'article', '内部机构', '', '', '', '', '', '40', '1', '1', '0', '0', '1', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('5', '2', '0,1,2,', '1', '3', 'article', '地方机构', '', '', '', '', '', '50', '1', '1', '0', '0', '1', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('6', '1', '0,1,', '1', '3', 'article', '质量检验', '', '', '', '', '', '20', '1', '1', '1', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('7', '6', '0,1,6,', '1', '3', 'article', '产品质量', '', '', '', '', '', '30', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('8', '6', '0,1,6,', '1', '3', 'article', '技术质量', '', '', '', '', '', '40', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_category` VALUES ('9', '6', '0,1,6,', '1', '3', 'article', '工程质量', '', '', '', '', '', '50', '1', '1', '0', '1', '0', null, null, null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);

-- ----------------------------
-- Table structure for cms_comment
-- ----------------------------
DROP TABLE IF EXISTS `cms_comment`;
CREATE TABLE `cms_comment` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `category_id` varchar(64) NOT NULL COMMENT '栏目编号',
  `content_id` varchar(64) NOT NULL COMMENT '栏目内容的编号',
  `title` varchar(255) DEFAULT NULL COMMENT '栏目内容的标题',
  `content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `name` varchar(100) DEFAULT NULL COMMENT '评论姓名',
  `ip` varchar(100) DEFAULT NULL COMMENT '评论IP',
  `create_date` datetime NOT NULL COMMENT '评论时间',
  `audit_user_id` varchar(64) DEFAULT NULL COMMENT '审核人',
  `audit_date` datetime DEFAULT NULL COMMENT '审核时间',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `cms_comment_category_id` (`category_id`) USING BTREE,
  KEY `cms_comment_content_id` (`content_id`) USING BTREE,
  KEY `cms_comment_status` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='评论表';

-- ----------------------------
-- Records of cms_comment
-- ----------------------------

-- ----------------------------
-- Table structure for cms_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `cms_guestbook`;
CREATE TABLE `cms_guestbook` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `type` char(1) NOT NULL COMMENT '留言分类',
  `content` varchar(255) NOT NULL COMMENT '留言内容',
  `name` varchar(100) NOT NULL COMMENT '姓名',
  `email` varchar(100) NOT NULL COMMENT '邮箱',
  `phone` varchar(100) NOT NULL COMMENT '电话',
  `workunit` varchar(100) NOT NULL COMMENT '单位',
  `ip` varchar(100) NOT NULL COMMENT 'IP',
  `create_date` datetime NOT NULL COMMENT '留言时间',
  `re_user_id` bigint(20) DEFAULT NULL COMMENT '回复人',
  `re_date` datetime DEFAULT NULL COMMENT '回复时间',
  `re_content` varchar(100) DEFAULT NULL COMMENT '回复内容',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `cms_guestbook_del_flag` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='留言板';

-- ----------------------------
-- Records of cms_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for cms_link
-- ----------------------------
DROP TABLE IF EXISTS `cms_link`;
CREATE TABLE `cms_link` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `category_id` varchar(64) NOT NULL COMMENT '栏目编号',
  `title` varchar(255) NOT NULL COMMENT '链接名称',
  `color` varchar(50) DEFAULT NULL COMMENT '标题颜色',
  `image` varchar(255) DEFAULT NULL COMMENT '链接图片',
  `href` varchar(255) DEFAULT NULL COMMENT '链接地址',
  `weight` int(11) DEFAULT '0' COMMENT '权重，越大越靠前',
  `weight_date` datetime DEFAULT NULL COMMENT '权重期限',
  `createBy` bigint(20) DEFAULT NULL COMMENT '创建者',
  `createAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updateBy` bigint(20) DEFAULT NULL COMMENT '更新者',
  `updateAt` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `deleted` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_link_category_id` (`category_id`) USING BTREE,
  KEY `cms_link_title` (`title`) USING BTREE,
  KEY `cms_link_del_flag` (`deleted`) USING BTREE,
  KEY `cms_link_weight` (`weight`) USING BTREE,
  KEY `cms_link_create_by` (`createBy`) USING BTREE,
  KEY `cms_link_update_date` (`updateAt`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='友情链接';

-- ----------------------------
-- Records of cms_link
-- ----------------------------
INSERT INTO `cms_link` VALUES ('1', '19', 'JeeSite', '', '', 'http://thinkgem.github.com/jeesite', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('10', '22', '58同城', '', '', 'http://www.58.com/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_link` VALUES ('11', '23', '视频大全', '', '', 'http://v.360.cn/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('12', '23', '凤凰网', '', '', 'http://www.ifeng.com/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('2', '19', 'ThinkGem', '', '', 'http://thinkgem.iteye.com/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('3', '19', '百度一下', '', '', 'http://www.baidu.com', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('4', '19', '谷歌搜索', '', '', 'http://www.google.com', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('5', '20', '新浪网', '', '', 'http://www.sina.com.cn', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('6', '20', '腾讯网', '', '', 'http://www.qq.com/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('7', '21', '淘宝网', '', '', 'http://www.taobao.com/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('8', '21', '新华网', '', '', 'http://www.xinhuanet.com/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);
INSERT INTO `cms_link` VALUES ('9', '22', '赶集网', '', '', 'http://www.ganji.com/', '0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '1', null);

-- ----------------------------
-- Table structure for cms_site
-- ----------------------------
DROP TABLE IF EXISTS `cms_site`;
CREATE TABLE `cms_site` (
  `id` varchar(64) NOT NULL COMMENT '编号',
  `name` varchar(100) NOT NULL COMMENT '站点名称',
  `title` varchar(100) NOT NULL COMMENT '站点标题',
  `logo` varchar(255) DEFAULT NULL COMMENT '站点Logo',
  `domain` varchar(255) DEFAULT NULL COMMENT '站点域名',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `keywords` varchar(255) DEFAULT NULL COMMENT '关键字',
  `theme` varchar(255) DEFAULT 'default' COMMENT '主题',
  `copyright` text COMMENT '版权信息',
  `custom_index_view` varchar(255) DEFAULT NULL COMMENT '自定义站点首页视图',
  `createBy` bigint(20) DEFAULT NULL COMMENT '创建者',
  `createAt` datetime DEFAULT NULL COMMENT '创建时间',
  `updateBy` bigint(20) DEFAULT NULL COMMENT '更新者',
  `updateAt` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `deleted` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cms_site_del_flag` (`deleted`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点表';

-- ----------------------------
-- Records of cms_site
-- ----------------------------
INSERT INTO `cms_site` VALUES ('1', '默认站点', 'Huluwa', null, null, 'Huluwa', 'Huluwa', 'basic', 'Copyright &copy; 2015 <a href=\'#\'>Huluwa</a> - Powered By Huluwa V1.0.0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);
INSERT INTO `cms_site` VALUES ('2', '子站点测试', 'Huluwa', null, null, 'Huluwa', 'Huluwa', 'basic', 'Copyright &copy; 2015 <a href=\'#\'>Huluwa</a> - Powered By Huluwa V1.0.0', null, '1', '2013-05-27 00:00:00', '1', '2013-05-27 00:00:00', '', '0', null);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `k` varchar(100) DEFAULT NULL COMMENT '键',
  `v` varchar(1000) DEFAULT NULL COMMENT '值',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `kvType` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('2', 'oss_qiniu', '{\"AccessKey\" : \"8-HMj9EgGNIP-xuOCpSzTn-OMyGOFtR3TxLdn4Uu\",\"SecretKey\" : \"SjpGg3V6PsMdJgn42PeEd5Ik-6aNyuwdqV5CPM6A\",\"bucket\" : \"ifast\",\"AccessUrl\" : \"http://p6r7ke2jc.bkt.clouddn.com/\"}', '七牛对象存储配置', '2018-04-06 14:31:26', '4300');
INSERT INTO `sys_config` VALUES ('3', 'author', 'kanchai', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('4', 'email', 'zhongzyong@163.com', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('5', 'package', 'com.boringsite', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('6', 'autoRemovePre', 'true', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('7', 'tablePrefix', 'app', '代码生成器配置', '2018-05-27 19:57:04', '4401');
INSERT INTO `sys_config` VALUES ('8', 'tinyint', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('9', 'smallint', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('10', 'mediumint', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('11', 'int', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('12', 'integer', 'Integer', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('13', 'bigint', 'Long', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('14', 'float', 'Float', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('15', 'double', 'Double', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('16', 'decimal', 'BigDecimal', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('17', 'bit', 'Boolean', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('18', 'char', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('19', 'varchar', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('20', 'tinytext', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('21', 'text', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('22', 'mediumtext', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('23', 'longtext', 'String', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('24', 'date', 'Date', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('25', 'datetime', 'Date', '代码生成器配置', '2018-05-27 19:57:04', '4400');
INSERT INTO `sys_config` VALUES ('26', 'timestamp', 'Date', '代码生成器配置', '2018-05-27 19:57:04', '4400');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parentId` bigint(20) DEFAULT NULL COMMENT '上级部门ID，一级部门为0',
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `orderNum` int(11) DEFAULT NULL COMMENT '排序',
  `delFlag` tinyint(4) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='部门管理';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('6', '0', '研发部', '1', '1');
INSERT INTO `sys_dept` VALUES ('7', '6', '研发一部', '1', '1');
INSERT INTO `sys_dept` VALUES ('8', '6', '研发二部', '2', '1');
INSERT INTO `sys_dept` VALUES ('9', '0', '销售部', '2', '1');
INSERT INTO `sys_dept` VALUES ('11', '0', '产品部', '3', '1');
INSERT INTO `sys_dept` VALUES ('12', '11', '产品一部', '1', '1');
INSERT INTO `sys_dept` VALUES ('13', '0', '测试部', '5', '1');
INSERT INTO `sys_dept` VALUES ('14', '13', '测试一部', '1', '1');
INSERT INTO `sys_dept` VALUES ('15', '13', '测试二部', '2', '1');
INSERT INTO `sys_dept` VALUES ('16', '9', '销售一部', '0', '1');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '标签名',
  `value` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '数据值',
  `type` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '类型',
  `description` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  `sort` decimal(10,0) DEFAULT NULL COMMENT '排序（升序）',
  `parentId` bigint(64) DEFAULT '0' COMMENT '父级编号',
  `createBy` int(64) DEFAULT NULL COMMENT '创建者',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  `updateBy` bigint(64) DEFAULT NULL COMMENT '更新者',
  `updateDate` datetime DEFAULT NULL COMMENT '更新时间',
  `remarks` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注信息',
  `delFlag` char(1) COLLATE utf8_bin DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_dict_value` (`value`),
  KEY `sys_dict_label` (`name`),
  KEY `sys_dict_del_flag` (`delFlag`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('1', '正常', '0', 'del_flag', '删除标记', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('3', '显示', '1', 'show_hide', '显示/隐藏', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('4', '隐藏', '0', 'show_hide', '显示/隐藏', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('5', '是', '1', 'yes_no', '是/否', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('6', '否', '0', 'yes_no', '是/否', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('7', '红色', 'red', 'color', '颜色值', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('8', '绿色', 'green', 'color', '颜色值', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('9', '蓝色', 'blue', 'color', '颜色值', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('10', '黄色', 'yellow', 'color', '颜色值', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('11', '橙色', 'orange', 'color', '颜色值', '50', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('12', '默认主题', 'default', 'theme', '主题方案', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('13', '天蓝主题', 'cerulean', 'theme', '主题方案', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('14', '橙色主题', 'readable', 'theme', '主题方案', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('15', '红色主题', 'united', 'theme', '主题方案', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('16', 'Flat主题', 'flat', 'theme', '主题方案', '60', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('17', '国家', '1', 'sys_area_type', '区域类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('18', '省份、直辖市', '2', 'sys_area_type', '区域类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('19', '地市', '3', 'sys_area_type', '区域类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('20', '区县', '4', 'sys_area_type', '区域类型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('21', '公司', '1', 'sys_office_type', '机构类型', '60', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('22', '部门', '2', 'sys_office_type', '机构类型', '70', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('23', '小组', '3', 'sys_office_type', '机构类型', '80', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('24', '其它', '4', 'sys_office_type', '机构类型', '90', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('25', '综合部', '1', 'sys_office_common', '快捷通用部门', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('26', '开发部', '2', 'sys_office_common', '快捷通用部门', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('27', '人力部', '3', 'sys_office_common', '快捷通用部门', '50', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('28', '一级', '1', 'sys_office_grade', '机构等级', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('29', '二级', '2', 'sys_office_grade', '机构等级', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('30', '三级', '3', 'sys_office_grade', '机构等级', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('31', '四级', '4', 'sys_office_grade', '机构等级', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('32', '所有数据', '1', 'sys_data_scope', '数据范围', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('33', '所在公司及以下数据', '2', 'sys_data_scope', '数据范围', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('34', '所在公司数据', '3', 'sys_data_scope', '数据范围', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('35', '所在部门及以下数据', '4', 'sys_data_scope', '数据范围', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('36', '所在部门数据', '5', 'sys_data_scope', '数据范围', '50', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('37', '仅本人数据', '8', 'sys_data_scope', '数据范围', '90', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('38', '按明细设置', '9', 'sys_data_scope', '数据范围', '100', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('39', '系统管理', '1', 'sys_user_type', '用户类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('40', '部门经理', '2', 'sys_user_type', '用户类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('41', '普通用户', '3', 'sys_user_type', '用户类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('42', '基础主题', 'basic', 'cms_theme', '站点主题', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('43', '蓝色主题', 'blue', 'cms_theme', '站点主题', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('44', '红色主题', 'red', 'cms_theme', '站点主题', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('45', '文章模型', 'article', 'cms_module', '栏目模型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('46', '图片模型', 'picture', 'cms_module', '栏目模型', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('47', '下载模型', 'download', 'cms_module', '栏目模型', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('48', '链接模型', 'link', 'cms_module', '栏目模型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('49', '专题模型', 'special', 'cms_module', '栏目模型', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('50', '默认展现方式', '0', 'cms_show_modes', '展现方式', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('51', '首栏目内容列表', '1', 'cms_show_modes', '展现方式', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('52', '栏目第一条内容', '2', 'cms_show_modes', '展现方式', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('53', '发布', '0', 'cms_del_flag', '内容状态', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('54', '删除', '1', 'cms_del_flag', '内容状态', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('55', '审核', '2', 'cms_del_flag', '内容状态', '15', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('56', '首页焦点图', '1', 'cms_posid', '推荐位', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('57', '栏目页文章推荐', '2', 'cms_posid', '推荐位', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('58', '咨询', '1', 'cms_guestbook', '留言板分类', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('59', '建议', '2', 'cms_guestbook', '留言板分类', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('60', '投诉', '3', 'cms_guestbook', '留言板分类', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('61', '其它', '4', 'cms_guestbook', '留言板分类', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('62', '公休', '1', 'oa_leave_type', '请假类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('63', '病假', '2', 'oa_leave_type', '请假类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('64', '事假', '3', 'oa_leave_type', '请假类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('65', '调休', '4', 'oa_leave_type', '请假类型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('66', '婚假', '5', 'oa_leave_type', '请假类型', '60', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('67', '接入日志', '1', 'sys_log_type', '日志类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('68', '异常日志', '2', 'sys_log_type', '日志类型', '40', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('69', '请假流程', 'leave', 'act_type', '流程类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('70', '审批测试流程', 'test_audit', 'act_type', '流程类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('71', '分类1', '1', 'act_category', '流程分类', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('72', '分类2', '2', 'act_category', '流程分类', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('73', '增删改查', 'crud', 'gen_category', '代码生成分类', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('74', '增删改查（包含从表）', 'crud_many', 'gen_category', '代码生成分类', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('75', '树结构', 'tree', 'gen_category', '代码生成分类', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('76', '=', '=', 'gen_query_type', '查询方式', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('77', '!=', '!=', 'gen_query_type', '查询方式', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('78', '&gt;', '&gt;', 'gen_query_type', '查询方式', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('79', '&lt;', '&lt;', 'gen_query_type', '查询方式', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('80', 'Between', 'between', 'gen_query_type', '查询方式', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('81', 'Like', 'like', 'gen_query_type', '查询方式', '60', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('82', 'Left Like', 'left_like', 'gen_query_type', '查询方式', '70', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('83', 'Right Like', 'right_like', 'gen_query_type', '查询方式', '80', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('84', '文本框', 'input', 'gen_show_type', '字段生成方案', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('85', '文本域', 'textarea', 'gen_show_type', '字段生成方案', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('86', '下拉框', 'select', 'gen_show_type', '字段生成方案', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('87', '复选框', 'checkbox', 'gen_show_type', '字段生成方案', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('88', '单选框', 'radiobox', 'gen_show_type', '字段生成方案', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('89', '日期选择', 'dateselect', 'gen_show_type', '字段生成方案', '60', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('90', '人员选择', 'userselect', 'gen_show_type', '字段生成方案', '70', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('91', '部门选择', 'officeselect', 'gen_show_type', '字段生成方案', '80', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('92', '区域选择', 'areaselect', 'gen_show_type', '字段生成方案', '90', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('93', 'String', 'String', 'gen_java_type', 'Java类型', '10', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('94', 'Long', 'Long', 'gen_java_type', 'Java类型', '20', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('95', '仅持久层', 'dao', 'gen_category', '代码生成分类\0\0', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('96', '男', '1', 'sex', '性别', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('97', '女', '2', 'sex', '性别', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('98', 'Integer', 'Integer', 'gen_java_type', 'Java类型', '30', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('99', 'Double', 'Double', 'gen_java_type', 'Java类型', '40', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('100', 'Date', 'java.util.Date', 'gen_java_type', 'Java类型', '50', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('104', 'Custom', 'Custom', 'gen_java_type', 'Java类型', '90', '0', '1', null, '1', null, null, '1');
INSERT INTO `sys_dict` VALUES ('105', '会议通告', '1', 'oa_notify_type', '通知通告类型', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('106', '奖惩通告', '2', 'oa_notify_type', '通知通告类型', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('107', '活动通告', '3', 'oa_notify_type', '通知通告类型', '30', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('108', '草稿', '0', 'oa_notify_status', '通知通告状态', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('109', '发布', '1', 'oa_notify_status', '通知通告状态', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('110', '未读', '0', 'oa_notify_read', '通知通告状态', '10', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('111', '已读', '1', 'oa_notify_read', '通知通告状态', '20', '0', '1', null, '1', null, null, '0');
INSERT INTO `sys_dict` VALUES ('112', '草稿', '0', 'oa_notify_status', '通知通告状态', '10', '0', '1', null, '1', null, '', '0');
INSERT INTO `sys_dict` VALUES ('113', '删除', '0', 'del_flag', '删除标记', null, null, null, null, null, null, '', '');
INSERT INTO `sys_dict` VALUES ('118', '关于', 'about', 'blog_type', '博客类型', null, null, null, null, null, null, '全url是:/blog/open/page/about', '');
INSERT INTO `sys_dict` VALUES ('119', '交流', 'communication', 'blog_type', '博客类型', null, null, null, null, null, null, '', '');
INSERT INTO `sys_dict` VALUES ('120', '文章', 'article', 'blog_type', '博客类型', null, null, null, null, null, null, '', '');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL COMMENT '文件类型',
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Records of sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL COMMENT '用户id',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `time` int(11) DEFAULT NULL COMMENT '响应时间',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1037963585972342787 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1037249529154121730', '-1', '{}', '重定向到登录', '0', 'LoginController.welcome()', '{}', '127.0.0.1', '2018-09-05 16:02:13');
INSERT INTO `sys_log` VALUES ('1037249548926070785', '1', 'admin', '登录', '55', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-05 16:02:18');
INSERT INTO `sys_log` VALUES ('1037249549278392321', '1', 'admin', '请求访问主页', '30', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-05 16:02:18');
INSERT INTO `sys_log` VALUES ('1037249549848817665', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 16:02:18');
INSERT INTO `sys_log` VALUES ('1037249550880616449', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 16:02:18');
INSERT INTO `sys_log` VALUES ('1037249562318483457', '1', 'admin', '进入个人中心', '94', 'UserController.personal()', '{}', '127.0.0.1', '2018-09-05 16:02:21');
INSERT INTO `sys_log` VALUES ('1037249724587716610', '1', 'admin', '更新用户', '22', 'UserController.updatePeronal()', '{\"id\":[\"1\"],\"name\":[\"超级管理员\"],\"sex\":[\"96\"],\"birth\":[\"2018-04-02\"],\"mobile\":[\"18620512233\"],\"email\":[\"zhongzyong@163.com\"],\"province\":[\"广东省\"],\"city\":[\"广州市\"],\"district\":[\"天河区\"],\"liveAddress\":[\"广州市天河区凌塘村\"],\"hobby\":[\"\"]}', '127.0.0.1', '2018-09-05 16:03:00');
INSERT INTO `sys_log` VALUES ('1037249895446884353', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-05 16:03:40');
INSERT INTO `sys_log` VALUES ('1037249896738729986', '1', 'admin', '查询数据表列表', '16', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-05 16:03:41');
INSERT INTO `sys_log` VALUES ('1037249918788186114', '1', 'admin', '进入数据字典列表页面', '0', 'com.ifast.common.controller.DictController.sysDict()', '{}', '127.0.0.1', '2018-09-05 16:03:46');
INSERT INTO `sys_log` VALUES ('1037249920457519105', '1', 'admin', '查询数据字典列表', '101', 'com.ifast.common.controller.DictController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"type\":[\"\"]}', '127.0.0.1', '2018-09-05 16:03:46');
INSERT INTO `sys_log` VALUES ('1037249939378024449', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 16:03:51');
INSERT INTO `sys_log` VALUES ('1037251739829153794', '1', 'admin', '请求访问主页', '11', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-05 16:11:00');
INSERT INTO `sys_log` VALUES ('1037251740395384833', '1', 'admin', '主页', '1', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 16:11:00');
INSERT INTO `sys_log` VALUES ('1037251741083250689', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 16:11:00');
INSERT INTO `sys_log` VALUES ('1037251758145679362', '1', 'admin', '进入在线用户列表页面', '10', 'SessionController.online()', '{}', '127.0.0.1', '2018-09-05 16:11:05');
INSERT INTO `sys_log` VALUES ('1037251758950985729', '1', 'admin', '查询在线用户列表数据', '7', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:05');
INSERT INTO `sys_log` VALUES ('1037251763480834049', '1', 'admin', '进入个人中心', '24', 'UserController.personal()', '{}', '127.0.0.1', '2018-09-05 16:11:06');
INSERT INTO `sys_log` VALUES ('1037251836130373634', '1', 'admin', '查询在线用户列表数据', '3', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:23');
INSERT INTO `sys_log` VALUES ('1037251838332383233', '1', 'admin', '查询在线用户列表数据', '2', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:24');
INSERT INTO `sys_log` VALUES ('1037251840274345985', '1', 'admin', '查询在线用户列表数据', '1', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:24');
INSERT INTO `sys_log` VALUES ('1037251841767518210', '1', 'admin', '查询在线用户列表数据', '4', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:24');
INSERT INTO `sys_log` VALUES ('1037251843751424002', '1', 'admin', '查询在线用户列表数据', '12', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:25');
INSERT INTO `sys_log` VALUES ('1037251846356086785', '1', 'admin', '查询在线用户列表数据', '2', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:26');
INSERT INTO `sys_log` VALUES ('1037251857861062657', '1', 'admin', '进入系统日志列表页面', '0', 'com.ifast.common.controller.LogController.log()', '{}', '127.0.0.1', '2018-09-05 16:11:28');
INSERT INTO `sys_log` VALUES ('1037251858746060801', '1', 'admin', '查询系统日志列表', '16', 'com.ifast.common.controller.LogController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"sort\":[\"gmtCreate\"],\"order\":[\"desc\"],\"operation\":[\"\"],\"username\":[\"\"]}', '127.0.0.1', '2018-09-05 16:11:28');
INSERT INTO `sys_log` VALUES ('1037251890161397762', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-05 16:11:36');
INSERT INTO `sys_log` VALUES ('1037251891000258562', '1', 'admin', '查询数据表列表', '10', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-05 16:11:36');
INSERT INTO `sys_log` VALUES ('1037252015357177858', '1', 'admin', '进入数据字典列表页面', '0', 'com.ifast.common.controller.DictController.sysDict()', '{}', '127.0.0.1', '2018-09-05 16:12:06');
INSERT INTO `sys_log` VALUES ('1037252016397365249', '1', 'admin', '查询数据字典列表', '10', 'com.ifast.common.controller.DictController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"type\":[\"\"]}', '127.0.0.1', '2018-09-05 16:12:06');
INSERT INTO `sys_log` VALUES ('1037252019576647681', '1', 'admin', '进入文件管理页面', '0', 'FileController.sysFile()', '{}', '127.0.0.1', '2018-09-05 16:12:07');
INSERT INTO `sys_log` VALUES ('1037252020377759746', '1', 'admin', '查询文件列表', '42', 'FileController.list()', '{\"type\":[\"\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-05 16:12:07');
INSERT INTO `sys_log` VALUES ('1037252025268318209', '1', 'admin', '查询文件列表', '6', 'FileController.list()', '{\"type\":[\"\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-05 16:12:08');
INSERT INTO `sys_log` VALUES ('1037252028284022786', '1', 'admin', '查询文件列表', '7', 'FileController.list()', '{\"type\":[\"0\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-05 16:12:09');
INSERT INTO `sys_log` VALUES ('1037252038274854913', '1', 'admin', '查询文件列表', '6', 'FileController.list()', '{\"type\":[\"99\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-05 16:12:11');
INSERT INTO `sys_log` VALUES ('1037264779584843778', '1', 'admin', '登录', '65', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-05 17:02:49');
INSERT INTO `sys_log` VALUES ('1037264779819724801', '1', 'admin', '请求访问主页', '27', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-05 17:02:49');
INSERT INTO `sys_log` VALUES ('1037264780809580546', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:02:49');
INSERT INTO `sys_log` VALUES ('1037264782193700866', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:02:50');
INSERT INTO `sys_log` VALUES ('1037265387624706050', '1', 'admin', '登录', '23', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-05 17:05:14');
INSERT INTO `sys_log` VALUES ('1037265387935084546', '1', 'admin', '请求访问主页', '28', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-05 17:05:14');
INSERT INTO `sys_log` VALUES ('1037265388719419394', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:05:14');
INSERT INTO `sys_log` VALUES ('1037265389419868161', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:05:14');
INSERT INTO `sys_log` VALUES ('1037265403676307457', '1', 'admin', '进入系统配置页面', '0', 'com.ifast.common.controller.ConfigController.Config()', '{}', '127.0.0.1', '2018-09-05 17:05:18');
INSERT INTO `sys_log` VALUES ('1037265405500829697', '1', 'admin', '查询系统配置列表', '15', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-05 17:05:18');
INSERT INTO `sys_log` VALUES ('1037265431530680321', '1', 'admin', '进入在线用户列表页面', '12', 'SessionController.online()', '{}', '127.0.0.1', '2018-09-05 17:05:24');
INSERT INTO `sys_log` VALUES ('1037265432600227841', '1', 'admin', '查询在线用户列表数据', '7', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-05 17:05:25');
INSERT INTO `sys_log` VALUES ('1037268007458287618', '1', 'admin', '登录', '13', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-05 17:15:39');
INSERT INTO `sys_log` VALUES ('1037268007613476866', '1', 'admin', '请求访问主页', '16', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-05 17:15:39');
INSERT INTO `sys_log` VALUES ('1037268008162930690', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:15:39');
INSERT INTO `sys_log` VALUES ('1037268009299587073', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:15:39');
INSERT INTO `sys_log` VALUES ('1037270002957135873', '1', 'admin', '登录', '16', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-05 17:23:34');
INSERT INTO `sys_log` VALUES ('1037270003070382082', '1', 'admin', '请求访问主页', '9', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-05 17:23:34');
INSERT INTO `sys_log` VALUES ('1037270003699527681', '1', 'admin', '主页', '1', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:23:35');
INSERT INTO `sys_log` VALUES ('1037270004861349890', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-05 17:23:35');
INSERT INTO `sys_log` VALUES ('1037273219271086081', '-1', '{}', '重定向到登录', '0', 'LoginController.welcome()', '{}', '127.0.0.1', '2018-09-05 17:36:21');
INSERT INTO `sys_log` VALUES ('1037529914631114753', '-1', '{}', '重定向到登录', '0', 'LoginController.welcome()', '{}', '127.0.0.1', '2018-09-06 10:36:22');
INSERT INTO `sys_log` VALUES ('1037529935237730306', '1', 'admin', '登录', '77', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 10:36:27');
INSERT INTO `sys_log` VALUES ('1037529935413891073', '1', 'admin', '请求访问主页', '22', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:36:27');
INSERT INTO `sys_log` VALUES ('1037529935954956290', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:36:27');
INSERT INTO `sys_log` VALUES ('1037529937028698113', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:36:28');
INSERT INTO `sys_log` VALUES ('1037534149863133186', '-1', '{}', '重定向到登录', '0', 'LoginController.welcome()', '{}', '127.0.0.1', '2018-09-06 10:53:12');
INSERT INTO `sys_log` VALUES ('1037534175708434434', '1', 'admin', '登录', '10', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 10:53:18');
INSERT INTO `sys_log` VALUES ('1037534175859429378', '1', 'admin', '请求访问主页', '15', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:53:18');
INSERT INTO `sys_log` VALUES ('1037534176928976898', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:53:18');
INSERT INTO `sys_log` VALUES ('1037534177079971841', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:53:18');
INSERT INTO `sys_log` VALUES ('1037534925096341506', '1', 'admin', '请求访问主页', '18', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:56:17');
INSERT INTO `sys_log` VALUES ('1037534926111363074', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:56:17');
INSERT INTO `sys_log` VALUES ('1037534927231242241', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:56:17');
INSERT INTO `sys_log` VALUES ('1037535516392542209', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:58:38');
INSERT INTO `sys_log` VALUES ('1037535516925218817', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:58:38');
INSERT INTO `sys_log` VALUES ('1037535517692776450', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:58:38');
INSERT INTO `sys_log` VALUES ('1037535523824848897', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:58:40');
INSERT INTO `sys_log` VALUES ('1037535524248473602', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:58:40');
INSERT INTO `sys_log` VALUES ('1037535524776955906', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:58:40');
INSERT INTO `sys_log` VALUES ('1037535556548808706', '1', 'admin', '请求访问主页', '17', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:58:47');
INSERT INTO `sys_log` VALUES ('1037535556842409985', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:58:47');
INSERT INTO `sys_log` VALUES ('1037535557362503682', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:58:48');
INSERT INTO `sys_log` VALUES ('1037535741886713858', '1', 'admin', '请求访问主页', '8', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:59:32');
INSERT INTO `sys_log` VALUES ('1037535742285172737', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:59:32');
INSERT INTO `sys_log` VALUES ('1037535743350525953', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:59:32');
INSERT INTO `sys_log` VALUES ('1037535747557412866', '1', 'admin', '请求访问主页', '9', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 10:59:33');
INSERT INTO `sys_log` VALUES ('1037535747842625538', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:59:33');
INSERT INTO `sys_log` VALUES ('1037535748299804674', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 10:59:33');
INSERT INTO `sys_log` VALUES ('1037536912814755841', '1', 'admin', '请求访问主页', '16', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:04:11');
INSERT INTO `sys_log` VALUES ('1037536913192243202', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:11');
INSERT INTO `sys_log` VALUES ('1037536913695559681', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:11');
INSERT INTO `sys_log` VALUES ('1037536918737113090', '1', 'admin', '请求访问主页', '7', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:04:12');
INSERT INTO `sys_log` VALUES ('1037536918955216898', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:12');
INSERT INTO `sys_log` VALUES ('1037536919315927042', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:12');
INSERT INTO `sys_log` VALUES ('1037536923438927874', '1', 'admin', '请求访问主页', '8', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:04:13');
INSERT INTO `sys_log` VALUES ('1037536923690586114', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:13');
INSERT INTO `sys_log` VALUES ('1037536924177125377', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:13');
INSERT INTO `sys_log` VALUES ('1037536929961070593', '1', 'admin', '请求访问主页', '18', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:04:15');
INSERT INTO `sys_log` VALUES ('1037536930086899713', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:15');
INSERT INTO `sys_log` VALUES ('1037536930544078850', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:15');
INSERT INTO `sys_log` VALUES ('1037536952488677377', '1', 'admin', '请求访问主页', '17', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:04:20');
INSERT INTO `sys_log` VALUES ('1037536952840998914', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:20');
INSERT INTO `sys_log` VALUES ('1037536953365286914', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:04:20');
INSERT INTO `sys_log` VALUES ('1037536964702490625', '-1', '{}', '重定向到登录', '0', 'LoginController.welcome()', '{}', '127.0.0.1', '2018-09-06 11:04:23');
INSERT INTO `sys_log` VALUES ('1037537442081394690', '1', 'admin', '登录', '6', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 11:06:17');
INSERT INTO `sys_log` VALUES ('1037537442261749761', '1', 'admin', '请求访问主页', '19', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:06:17');
INSERT INTO `sys_log` VALUES ('1037537442588905473', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:06:17');
INSERT INTO `sys_log` VALUES ('1037537443155136513', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:06:17');
INSERT INTO `sys_log` VALUES ('1037537501380464642', '1', 'admin', '进入数据字典列表页面', '1', 'com.ifast.common.controller.DictController.sysDict()', '{}', '127.0.0.1', '2018-09-06 11:06:31');
INSERT INTO `sys_log` VALUES ('1037537503624417282', '1', 'admin', '查询数据字典列表', '59', 'com.ifast.common.controller.DictController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"type\":[\"\"]}', '127.0.0.1', '2018-09-06 11:06:32');
INSERT INTO `sys_log` VALUES ('1037537518354812930', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:06:35');
INSERT INTO `sys_log` VALUES ('1037542279451889665', '1', 'admin', '登录', '6', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 11:25:30');
INSERT INTO `sys_log` VALUES ('1037542279548358658', '1', 'admin', '请求访问主页', '7', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:25:30');
INSERT INTO `sys_log` VALUES ('1037542279883902977', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:25:30');
INSERT INTO `sys_log` VALUES ('1037542280332693506', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:25:30');
INSERT INTO `sys_log` VALUES ('1037542297936187394', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:25:35');
INSERT INTO `sys_log` VALUES ('1037542883175813122', '1', 'admin', '请求访问主页', '10', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:27:54');
INSERT INTO `sys_log` VALUES ('1037542883402305538', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:27:54');
INSERT INTO `sys_log` VALUES ('1037542884232777729', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:27:54');
INSERT INTO `sys_log` VALUES ('1037543146204811266', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:28:57');
INSERT INTO `sys_log` VALUES ('1037543199669604354', '1', 'admin', '进入数据字典列表页面', '0', 'com.ifast.common.controller.DictController.sysDict()', '{}', '127.0.0.1', '2018-09-06 11:29:10');
INSERT INTO `sys_log` VALUES ('1037543201062113282', '1', 'admin', '查询数据字典列表', '46', 'com.ifast.common.controller.DictController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"type\":[\"\"]}', '127.0.0.1', '2018-09-06 11:29:10');
INSERT INTO `sys_log` VALUES ('1037543247048462338', '1', 'admin', '请求访问主页', '7', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 11:29:21');
INSERT INTO `sys_log` VALUES ('1037543247354646530', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:29:21');
INSERT INTO `sys_log` VALUES ('1037543247941849090', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 11:29:21');
INSERT INTO `sys_log` VALUES ('1037585521627222018', '1', 'admin', '登录', '5', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 14:17:20');
INSERT INTO `sys_log` VALUES ('1037585521711108097', '1', 'admin', '请求访问主页', '8', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 14:17:20');
INSERT INTO `sys_log` VALUES ('1037585522147315714', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:17:20');
INSERT INTO `sys_log` VALUES ('1037585522583523330', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:17:20');
INSERT INTO `sys_log` VALUES ('1037585628753940481', '1', 'admin', '请求访问主页', '11', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 14:17:45');
INSERT INTO `sys_log` VALUES ('1037585629081096194', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:17:46');
INSERT INTO `sys_log` VALUES ('1037585629563441153', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:17:46');
INSERT INTO `sys_log` VALUES ('1037589258722725889', '-1', '{\"username\":[\"admin\"],\"password\":[\"admin\"]}', '登录', '18', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"admin\"]}', '127.0.0.1', '2018-09-06 14:32:11');
INSERT INTO `sys_log` VALUES ('1037589271494381570', '1', 'admin', '登录', '9', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 14:32:14');
INSERT INTO `sys_log` VALUES ('1037589271595044865', '1', 'admin', '请求访问主页', '12', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 14:32:14');
INSERT INTO `sys_log` VALUES ('1037589271838314498', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:32:14');
INSERT INTO `sys_log` VALUES ('1037589272358408193', '1', 'admin', '主页', '1', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:32:14');
INSERT INTO `sys_log` VALUES ('1037591299369062402', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 14:40:17');
INSERT INTO `sys_log` VALUES ('1037591299679440897', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:40:18');
INSERT INTO `sys_log` VALUES ('1037591300258254849', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 14:40:18');
INSERT INTO `sys_log` VALUES ('1037597016721313794', '1', 'admin', '登录', '5', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 15:03:01');
INSERT INTO `sys_log` VALUES ('1037597016834560001', '1', 'admin', '请求访问主页', '15', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 15:03:01');
INSERT INTO `sys_log` VALUES ('1037597017098801154', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:03:01');
INSERT INTO `sys_log` VALUES ('1037597017560174593', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:03:01');
INSERT INTO `sys_log` VALUES ('1037597392098938881', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:04:30');
INSERT INTO `sys_log` VALUES ('1037598410320760834', '1', 'admin', '请求访问主页', '7', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 15:08:33');
INSERT INTO `sys_log` VALUES ('1037598411205758977', '1', 'admin', '主页', '1', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:08:33');
INSERT INTO `sys_log` VALUES ('1037598411633577986', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:08:33');
INSERT INTO `sys_log` VALUES ('1037598462896361473', '1', 'admin', '登录', '20', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 15:08:45');
INSERT INTO `sys_log` VALUES ('1037598462967664641', '1', 'admin', '请求访问主页', '11', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 15:08:45');
INSERT INTO `sys_log` VALUES ('1037598463324180482', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:08:45');
INSERT INTO `sys_log` VALUES ('1037598464074960898', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:08:46');
INSERT INTO `sys_log` VALUES ('1037599834672844801', '1', 'admin', '进入系统用户列表页面', '0', 'UserController.user()', '{}', '127.0.0.1', '2018-09-06 15:14:12');
INSERT INTO `sys_log` VALUES ('1037599835834667010', '1', 'admin', '查询部门树形数据', '15', 'DeptController.tree()', '{}', '127.0.0.1', '2018-09-06 15:14:13');
INSERT INTO `sys_log` VALUES ('1037599836207960066', '1', 'admin', '查询系统用户列表', '36', 'UserController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"name\":[\"\"],\"deptId\":[\"\"]}', '127.0.0.1', '2018-09-06 15:14:13');
INSERT INTO `sys_log` VALUES ('1037599846446256129', '1', 'admin', '进入定时任务管理页面', '0', 'JobController.taskScheduleJob()', '{}', '127.0.0.1', '2018-09-06 15:14:15');
INSERT INTO `sys_log` VALUES ('1037599847478054913', '1', 'admin', '查询定时任务列表', '32', 'JobController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-06 15:14:15');
INSERT INTO `sys_log` VALUES ('1037599869632368641', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 15:14:21');
INSERT INTO `sys_log` VALUES ('1037599870391537665', '1', 'admin', '查询数据表列表', '12', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 15:14:21');
INSERT INTO `sys_log` VALUES ('1037599891056873474', '1', 'admin', '查询数据表列表', '7', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 15:14:26');
INSERT INTO `sys_log` VALUES ('1037600059412041729', '-1', '{}', '重定向到登录', '0', 'LoginController.welcome()', '{}', '127.0.0.1', '2018-09-06 15:15:06');
INSERT INTO `sys_log` VALUES ('1037600079624392706', '1', 'admin', '登录', '12', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 15:15:11');
INSERT INTO `sys_log` VALUES ('1037600079708278785', '1', 'admin', '请求访问主页', '6', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 15:15:11');
INSERT INTO `sys_log` VALUES ('1037600080056406017', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:15:11');
INSERT INTO `sys_log` VALUES ('1037600080383561730', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 15:15:11');
INSERT INTO `sys_log` VALUES ('1037619094853898242', '1', 'admin', '登录', '5', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 16:30:44');
INSERT INTO `sys_log` VALUES ('1037619094937784322', '1', 'admin', '请求访问主页', '8', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 16:30:44');
INSERT INTO `sys_log` VALUES ('1037619095164276737', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 16:30:44');
INSERT INTO `sys_log` VALUES ('1037619095847948290', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 16:30:45');
INSERT INTO `sys_log` VALUES ('1037620532581507074', '1', 'admin', '登录', '34', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 16:36:27');
INSERT INTO `sys_log` VALUES ('1037620532854136833', '1', 'admin', '请求访问主页', '21', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 16:36:27');
INSERT INTO `sys_log` VALUES ('1037620533495865345', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 16:36:27');
INSERT INTO `sys_log` VALUES ('1037620534397640706', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 16:36:28');
INSERT INTO `sys_log` VALUES ('1037620884974346242', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:37:51');
INSERT INTO `sys_log` VALUES ('1037620887314767874', '1', 'admin', '查询数据表列表', '17', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:37:52');
INSERT INTO `sys_log` VALUES ('1037620938028097537', '1', 'admin', '根据数据表批量生成代码', '533', 'GeneratorController.batchCode()', '{\"tables\":[\"cms_site\"]}', '127.0.0.1', '2018-09-06 16:38:04');
INSERT INTO `sys_log` VALUES ('1037623744311074818', '1', 'admin', '登录', '8', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 16:49:13');
INSERT INTO `sys_log` VALUES ('1037623744424321026', '1', 'admin', '请求访问主页', '10', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 16:49:13');
INSERT INTO `sys_log` VALUES ('1037623745032495106', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 16:49:13');
INSERT INTO `sys_log` VALUES ('1037623746689245186', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 16:49:13');
INSERT INTO `sys_log` VALUES ('1037623754893303809', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:49:15');
INSERT INTO `sys_log` VALUES ('1037623756667494401', '1', 'admin', '查询数据表列表', '14', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:49:16');
INSERT INTO `sys_log` VALUES ('1037623917036707841', '1', 'admin', '根据数据表批量生成代码', '29267', 'GeneratorController.batchCode()', '{\"tables\":[\"cms_site\"]}', '127.0.0.1', '2018-09-06 16:49:54');
INSERT INTO `sys_log` VALUES ('1037624071961714689', '1', 'admin', '根据数据表批量生成代码', '30142', 'GeneratorController.batchCode()', '{\"tables\":[\"cms_site\"]}', '127.0.0.1', '2018-09-06 16:50:31');
INSERT INTO `sys_log` VALUES ('1037624434966142978', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:51:58');
INSERT INTO `sys_log` VALUES ('1037624436203462658', '1', 'admin', '查询数据表列表', '16', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:51:58');
INSERT INTO `sys_log` VALUES ('1037624549776826370', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:52:25');
INSERT INTO `sys_log` VALUES ('1037624551064477697', '1', 'admin', '查询数据表列表', '17', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:52:25');
INSERT INTO `sys_log` VALUES ('1037624597470257153', '1', 'admin', '进入定时任务管理页面', '0', 'JobController.taskScheduleJob()', '{}', '127.0.0.1', '2018-09-06 16:52:36');
INSERT INTO `sys_log` VALUES ('1037624599055704066', '1', 'admin', '查询定时任务列表', '76', 'JobController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-06 16:52:37');
INSERT INTO `sys_log` VALUES ('1037624612737523713', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:52:40');
INSERT INTO `sys_log` VALUES ('1037624613949677570', '1', 'admin', '查询数据表列表', '9', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:52:40');
INSERT INTO `sys_log` VALUES ('1037624727250411521', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:53:07');
INSERT INTO `sys_log` VALUES ('1037624728319959042', '1', 'admin', '查询数据表列表', '9', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:53:08');
INSERT INTO `sys_log` VALUES ('1037624957828079618', '1', 'admin', '进入代码生成页面', '1', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:54:02');
INSERT INTO `sys_log` VALUES ('1037624958855684098', '1', 'admin', '查询数据表列表', '9', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:54:02');
INSERT INTO `sys_log` VALUES ('1037625207766654978', '1', 'admin', '进入代码生成页面', '1', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:55:02');
INSERT INTO `sys_log` VALUES ('1037625208966225921', '1', 'admin', '查询数据表列表', '10', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:55:02');
INSERT INTO `sys_log` VALUES ('1037625335768424449', '1', 'admin', '进入代码生成页面', '0', 'GeneratorController.generator()', '{}', '127.0.0.1', '2018-09-06 16:55:32');
INSERT INTO `sys_log` VALUES ('1037625336829583361', '1', 'admin', '查询数据表列表', '8', 'GeneratorController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 16:55:33');
INSERT INTO `sys_log` VALUES ('1037625398041255938', '1', 'admin', '根据数据表批量生成代码', '6331', 'GeneratorController.batchCode()', '{\"tables\":[\"cms_article,cms_article_data,cms_category,cms_comment,cms_guestbook,cms_link,cms_site\"]}', '127.0.0.1', '2018-09-06 16:55:47');
INSERT INTO `sys_log` VALUES ('1037638561482772482', '1', 'admin', '登录', '35', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 17:48:06');
INSERT INTO `sys_log` VALUES ('1037638561726042114', '1', 'admin', '请求访问主页', '25', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 17:48:06');
INSERT INTO `sys_log` VALUES ('1037638562275495937', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:48:06');
INSERT INTO `sys_log` VALUES ('1037638563131133953', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:48:06');
INSERT INTO `sys_log` VALUES ('1037638579862212610', '1', 'admin', '进入系统角色页面', '0', 'RoleController.role()', '{}', '127.0.0.1', '2018-09-06 17:48:10');
INSERT INTO `sys_log` VALUES ('1037638581388939265', '1', 'admin', '查询系统角色菜单', '4', 'RoleController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 17:48:10');
INSERT INTO `sys_log` VALUES ('1037638589857239041', '1', 'admin', '编辑角色', '5', 'RoleController.edit()', '{}', '127.0.0.1', '2018-09-06 17:48:12');
INSERT INTO `sys_log` VALUES ('1037638590918397953', '1', 'admin', '根据角色ID查询菜单树形数据', '29', 'MenuController.tree()', '{}', '127.0.0.1', '2018-09-06 17:48:13');
INSERT INTO `sys_log` VALUES ('1037638637676498946', '1', 'admin', '进入系统菜单页面', '0', 'MenuController.menu()', '{}', '127.0.0.1', '2018-09-06 17:48:24');
INSERT INTO `sys_log` VALUES ('1037638638943178754', '1', 'admin', '查询菜单列表', '12', 'MenuController.list()', '{}', '127.0.0.1', '2018-09-06 17:48:24');
INSERT INTO `sys_log` VALUES ('1037638695557894146', '1', 'admin', '进入系统配置页面', '0', 'com.ifast.common.controller.ConfigController.Config()', '{}', '127.0.0.1', '2018-09-06 17:48:38');
INSERT INTO `sys_log` VALUES ('1037638696942014466', '1', 'admin', '查询系统配置列表', '52', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-06 17:48:38');
INSERT INTO `sys_log` VALUES ('1037638703359299585', '1', 'admin', '进入数据字典列表页面', '0', 'com.ifast.common.controller.DictController.sysDict()', '{}', '127.0.0.1', '2018-09-06 17:48:39');
INSERT INTO `sys_log` VALUES ('1037638704672116737', '1', 'admin', '查询数据字典列表', '17', 'com.ifast.common.controller.DictController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"type\":[\"\"]}', '127.0.0.1', '2018-09-06 17:48:40');
INSERT INTO `sys_log` VALUES ('1037638707134173186', '1', 'admin', '进入文件管理页面', '0', 'FileController.sysFile()', '{}', '127.0.0.1', '2018-09-06 17:48:40');
INSERT INTO `sys_log` VALUES ('1037638707889147906', '1', 'admin', '查询文件列表', '29', 'FileController.list()', '{\"type\":[\"\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-06 17:48:41');
INSERT INTO `sys_log` VALUES ('1037638713186553857', '1', 'admin', '进入部分页面', '1', 'DeptController.dept()', '{}', '127.0.0.1', '2018-09-06 17:48:42');
INSERT INTO `sys_log` VALUES ('1037638714281267202', '1', 'admin', '获取部门列表', '9', 'DeptController.list()', '{}', '127.0.0.1', '2018-09-06 17:48:42');
INSERT INTO `sys_log` VALUES ('1037638776973529089', '1', 'admin', '添加菜单', '0', 'MenuController.add()', '{}', '127.0.0.1', '2018-09-06 17:48:57');
INSERT INTO `sys_log` VALUES ('1037639396115714049', '1', 'admin', '保存菜单', '10', 'MenuController.save()', '{\"parentId\":[\"0\"],\"type\":[\"0\"],\"name\":[\"cms管理\"],\"url\":[\"\"],\"perms\":[\"\"],\"orderNum\":[\"\"],\"icon\":[\"fa fa-newspaper-o\"]}', '127.0.0.1', '2018-09-06 17:51:25');
INSERT INTO `sys_log` VALUES ('1037639396283486210', '1', 'admin', '查询菜单列表', '6', 'MenuController.list()', '{}', '127.0.0.1', '2018-09-06 17:51:25');
INSERT INTO `sys_log` VALUES ('1037639784235634689', '1', 'admin', '进入系统菜单页面', '0', 'MenuController.menu()', '{}', '127.0.0.1', '2018-09-06 17:52:57');
INSERT INTO `sys_log` VALUES ('1037639784986415105', '1', 'admin', '查询菜单列表', '29', 'MenuController.list()', '{}', '127.0.0.1', '2018-09-06 17:52:57');
INSERT INTO `sys_log` VALUES ('1037639813260218370', '1', 'admin', '添加菜单', '16', 'MenuController.add()', '{}', '127.0.0.1', '2018-09-06 17:53:04');
INSERT INTO `sys_log` VALUES ('1037639839994712065', '1', 'admin', '编辑菜单', '7', 'MenuController.edit()', '{}', '127.0.0.1', '2018-09-06 17:53:10');
INSERT INTO `sys_log` VALUES ('1037640035541553154', '1', 'admin', '进入系统菜单页面', '0', 'MenuController.menu()', '{}', '127.0.0.1', '2018-09-06 17:53:57');
INSERT INTO `sys_log` VALUES ('1037640036535603202', '1', 'admin', '查询菜单列表', '4', 'MenuController.list()', '{}', '127.0.0.1', '2018-09-06 17:53:57');
INSERT INTO `sys_log` VALUES ('1037640069347643393', '1', 'admin', '进入系统用户列表页面', '0', 'UserController.user()', '{}', '127.0.0.1', '2018-09-06 17:54:05');
INSERT INTO `sys_log` VALUES ('1037640070203281410', '1', 'admin', '查询部门树形数据', '15', 'DeptController.tree()', '{}', '127.0.0.1', '2018-09-06 17:54:05');
INSERT INTO `sys_log` VALUES ('1037640070358470657', '1', 'admin', '查询系统用户列表', '19', 'UserController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"name\":[\"\"],\"deptId\":[\"\"]}', '127.0.0.1', '2018-09-06 17:54:05');
INSERT INTO `sys_log` VALUES ('1037640084757516290', '1', 'admin', '编辑角色', '4', 'RoleController.edit()', '{}', '127.0.0.1', '2018-09-06 17:54:09');
INSERT INTO `sys_log` VALUES ('1037640085483130881', '1', 'admin', '根据角色ID查询菜单树形数据', '12', 'MenuController.tree()', '{}', '127.0.0.1', '2018-09-06 17:54:09');
INSERT INTO `sys_log` VALUES ('1037640111991132162', '1', 'admin', '更新角色', '48', 'RoleController.update()', '{\"id\":[\"1\"],\"menuIds\":[\"193,194,195,196,197,79,80,81,83,176,177,178,179,180,207,208,209,210,211,20,21,22,61,12,13,14,24,25,26,15,55,56,62,74,75,76,48,72,28,29,30,57,92,71,78,175,206,1,2,6,7,73,3,77,27,91,256,215,216,217,218,219,221,222,223,224,225,227,228,229,230,231,233,234,235,236,237,239,240,241,242,243,245,246,247,248,249,251,252,253,254,255,214,220,226,232,238,244,250,-1\"],\"roleName\":[\"超级用户角色\"],\"remark\":[\"超级管理员\"]}', '127.0.0.1', '2018-09-06 17:54:15');
INSERT INTO `sys_log` VALUES ('1037640112364425218', '1', 'admin', '查询系统角色菜单', '3', 'RoleController.list()', '{\"order\":[\"asc\"],\"limit\":[\"10\"],\"offset\":[\"0\"]}', '127.0.0.1', '2018-09-06 17:54:15');
INSERT INTO `sys_log` VALUES ('1037640119654125570', '1', 'admin', '请求访问主页', '13', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 17:54:17');
INSERT INTO `sys_log` VALUES ('1037640120073555970', '1', 'admin', '主页', '1', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:54:17');
INSERT INTO `sys_log` VALUES ('1037640120627204098', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:54:17');
INSERT INTO `sys_log` VALUES ('1037640407261745154', '1', 'admin', '请求访问主页', '15', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 17:55:26');
INSERT INTO `sys_log` VALUES ('1037640407588900866', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:55:26');
INSERT INTO `sys_log` VALUES ('1037640408259989506', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:55:26');
INSERT INTO `sys_log` VALUES ('1037640794265980930', '1', 'admin', '登录', '18', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 17:56:58');
INSERT INTO `sys_log` VALUES ('1037640794379227137', '1', 'admin', '请求访问主页', '11', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 17:56:58');
INSERT INTO `sys_log` VALUES ('1037640794756714498', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:56:58');
INSERT INTO `sys_log` VALUES ('1037640795385860098', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 17:56:58');
INSERT INTO `sys_log` VALUES ('1037643717188358145', '1', 'admin', '登录', '15', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 18:08:35');
INSERT INTO `sys_log` VALUES ('1037643717293215745', '1', 'admin', '请求访问主页', '7', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 18:08:35');
INSERT INTO `sys_log` VALUES ('1037643717565845506', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 18:08:35');
INSERT INTO `sys_log` VALUES ('1037643718060773377', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 18:08:35');
INSERT INTO `sys_log` VALUES ('1037644919519109122', '1', 'admin', '登录', '31', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-06 18:13:21');
INSERT INTO `sys_log` VALUES ('1037644919741407234', '1', 'admin', '请求访问主页', '24', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 18:13:22');
INSERT INTO `sys_log` VALUES ('1037644920316026882', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 18:13:22');
INSERT INTO `sys_log` VALUES ('1037644921150693377', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 18:13:22');
INSERT INTO `sys_log` VALUES ('1037645969948991490', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-06 18:17:32');
INSERT INTO `sys_log` VALUES ('1037645970624274434', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 18:17:32');
INSERT INTO `sys_log` VALUES ('1037645971572187137', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-06 18:17:32');
INSERT INTO `sys_log` VALUES ('1037877825441161217', '1', 'admin', '登录', '33', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 09:38:51');
INSERT INTO `sys_log` VALUES ('1037877825596350466', '1', 'admin', '请求访问主页', '12', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 09:38:51');
INSERT INTO `sys_log` VALUES ('1037877826179358722', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 09:38:51');
INSERT INTO `sys_log` VALUES ('1037877827014025217', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 09:38:51');
INSERT INTO `sys_log` VALUES ('1037880396046196737', '1', 'admin', '进入系统配置页面', '0', 'com.ifast.common.controller.ConfigController.Config()', '{}', '127.0.0.1', '2018-09-07 09:49:03');
INSERT INTO `sys_log` VALUES ('1037880397820387329', '1', 'admin', '查询系统配置列表', '60', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-07 09:49:04');
INSERT INTO `sys_log` VALUES ('1037880415390326785', '1', 'admin', '进入配置编辑页面', '3', 'com.ifast.common.controller.ConfigController.edit()', '{}', '127.0.0.1', '2018-09-07 09:49:08');
INSERT INTO `sys_log` VALUES ('1037880438173786113', '1', 'admin', '进入配置编辑页面', '3', 'com.ifast.common.controller.ConfigController.edit()', '{}', '127.0.0.1', '2018-09-07 09:49:13');
INSERT INTO `sys_log` VALUES ('1037880467684909057', '1', 'admin', '更新系统配置', '14', 'com.ifast.common.controller.ConfigController.update()', '{\"id\":[\"3\"],\"k\":[\"author\"],\"v\":[\"kanchai\"],\"remark\":[\"代码生成器配置\"],\"createTime\":[\"Sun May 27 19:57:04 CST 2018\"]}', '127.0.0.1', '2018-09-07 09:49:21');
INSERT INTO `sys_log` VALUES ('1037880467890429954', '1', 'admin', '查询系统配置列表', '15', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"k\":[\"\"]}', '127.0.0.1', '2018-09-07 09:49:21');
INSERT INTO `sys_log` VALUES ('1037880478476853250', '1', 'admin', '进入配置编辑页面', '4', 'com.ifast.common.controller.ConfigController.edit()', '{}', '127.0.0.1', '2018-09-07 09:49:23');
INSERT INTO `sys_log` VALUES ('1037880504166965250', '1', 'admin', '更新系统配置', '7', 'com.ifast.common.controller.ConfigController.update()', '{\"id\":[\"4\"],\"k\":[\"email\"],\"v\":[\"zhongzyong@163.com\"],\"remark\":[\"代码生成器配置\"],\"createTime\":[\"Sun May 27 19:57:04 CST 2018\"]}', '127.0.0.1', '2018-09-07 09:49:29');
INSERT INTO `sys_log` VALUES ('1037880504355708929', '1', 'admin', '查询系统配置列表', '14', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"k\":[\"\"]}', '127.0.0.1', '2018-09-07 09:49:29');
INSERT INTO `sys_log` VALUES ('1037880530062598146', '1', 'admin', '进入配置编辑页面', '4', 'com.ifast.common.controller.ConfigController.edit()', '{}', '127.0.0.1', '2018-09-07 09:49:35');
INSERT INTO `sys_log` VALUES ('1037880556218277890', '1', 'admin', '更新系统配置', '6', 'com.ifast.common.controller.ConfigController.update()', '{\"id\":[\"5\"],\"k\":[\"package\"],\"v\":[\"com.boringsite\"],\"remark\":[\"代码生成器配置\"],\"createTime\":[\"Sun May 27 19:57:04 CST 2018\"]}', '127.0.0.1', '2018-09-07 09:49:42');
INSERT INTO `sys_log` VALUES ('1037880556398632961', '1', 'admin', '查询系统配置列表', '11', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"k\":[\"\"]}', '127.0.0.1', '2018-09-07 09:49:42');
INSERT INTO `sys_log` VALUES ('1037880574652243970', '1', 'admin', '查询系统配置列表', '20', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"2\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-07 09:49:46');
INSERT INTO `sys_log` VALUES ('1037880590569627649', '1', 'admin', '查询系统配置列表', '6', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"3\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-07 09:49:50');
INSERT INTO `sys_log` VALUES ('1037880625034223618', '1', 'admin', '进入数据字典列表页面', '0', 'com.ifast.common.controller.DictController.sysDict()', '{}', '127.0.0.1', '2018-09-07 09:49:58');
INSERT INTO `sys_log` VALUES ('1037880626720333825', '1', 'admin', '查询数据字典列表', '23', 'com.ifast.common.controller.DictController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"type\":[\"\"]}', '127.0.0.1', '2018-09-07 09:49:58');
INSERT INTO `sys_log` VALUES ('1037880644483211265', '1', 'admin', '进入定时任务管理页面', '0', 'JobController.taskScheduleJob()', '{}', '127.0.0.1', '2018-09-07 09:50:03');
INSERT INTO `sys_log` VALUES ('1037880646123184130', '1', 'admin', '查询定时任务列表', '48', 'JobController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-07 09:50:03');
INSERT INTO `sys_log` VALUES ('1037880654037835777', '1', 'admin', '进入系统日志列表页面', '0', 'com.ifast.common.controller.LogController.log()', '{}', '127.0.0.1', '2018-09-07 09:50:05');
INSERT INTO `sys_log` VALUES ('1037880655396790274', '1', 'admin', '查询系统日志列表', '21', 'com.ifast.common.controller.LogController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"sort\":[\"gmtCreate\"],\"order\":[\"desc\"],\"operation\":[\"\"],\"username\":[\"\"]}', '127.0.0.1', '2018-09-07 09:50:05');
INSERT INTO `sys_log` VALUES ('1037881352410423297', '1', 'admin', '登录', '21', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 09:52:51');
INSERT INTO `sys_log` VALUES ('1037881352607555586', '1', 'admin', '请求访问主页', '22', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 09:52:52');
INSERT INTO `sys_log` VALUES ('1037881353152815105', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 09:52:52');
INSERT INTO `sys_log` VALUES ('1037881353870041089', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 09:52:52');
INSERT INTO `sys_log` VALUES ('1037882567546458113', '1', 'admin', '登录', '42', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 09:57:41');
INSERT INTO `sys_log` VALUES ('1037882567668092930', '1', 'admin', '请求访问主页', '20', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 09:57:41');
INSERT INTO `sys_log` VALUES ('1037882568448233474', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 09:57:41');
INSERT INTO `sys_log` VALUES ('1037882569924628481', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 09:57:42');
INSERT INTO `sys_log` VALUES ('1037883783034761218', '1', 'admin', '登录', '45', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:02:31');
INSERT INTO `sys_log` VALUES ('1037883783139618817', '1', 'admin', '请求访问主页', '25', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:02:31');
INSERT INTO `sys_log` VALUES ('1037883783886204930', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:02:31');
INSERT INTO `sys_log` VALUES ('1037883786478284801', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:02:32');
INSERT INTO `sys_log` VALUES ('1037884539938897921', '1', 'admin', '登录', '38', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:05:31');
INSERT INTO `sys_log` VALUES ('1037884540161196034', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:05:31');
INSERT INTO `sys_log` VALUES ('1037884540836478977', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:05:32');
INSERT INTO `sys_log` VALUES ('1037884542715527170', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:05:32');
INSERT INTO `sys_log` VALUES ('1037886138451066881', '1', 'admin', '进入系统配置页面', '0', 'com.ifast.common.controller.ConfigController.Config()', '{}', '127.0.0.1', '2018-09-07 10:11:53');
INSERT INTO `sys_log` VALUES ('1037886140615327745', '1', 'admin', '查询系统配置列表', '59', 'com.ifast.common.controller.ConfigController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"]}', '127.0.0.1', '2018-09-07 10:11:53');
INSERT INTO `sys_log` VALUES ('1037886159363866626', '1', 'admin', '进入系统菜单页面', '0', 'MenuController.menu()', '{}', '127.0.0.1', '2018-09-07 10:11:58');
INSERT INTO `sys_log` VALUES ('1037886161326800898', '1', 'admin', '查询菜单列表', '17', 'MenuController.list()', '{}', '127.0.0.1', '2018-09-07 10:11:58');
INSERT INTO `sys_log` VALUES ('1037886221657669633', '1', 'admin', '进入部分页面', '0', 'DeptController.dept()', '{}', '127.0.0.1', '2018-09-07 10:12:12');
INSERT INTO `sys_log` VALUES ('1037886222836269058', '1', 'admin', '获取部门列表', '11', 'DeptController.list()', '{}', '127.0.0.1', '2018-09-07 10:12:13');
INSERT INTO `sys_log` VALUES ('1037886262459858946', '1', 'admin', '进入系统日志列表页面', '0', 'com.ifast.common.controller.LogController.log()', '{}', '127.0.0.1', '2018-09-07 10:12:22');
INSERT INTO `sys_log` VALUES ('1037886263244193794', '1', 'admin', '查询系统日志列表', '17', 'com.ifast.common.controller.LogController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"sort\":[\"gmtCreate\"],\"order\":[\"desc\"],\"operation\":[\"\"],\"username\":[\"\"]}', '127.0.0.1', '2018-09-07 10:12:22');
INSERT INTO `sys_log` VALUES ('1037886295154458625', '1', 'admin', '进入在线用户列表页面', '16', 'SessionController.online()', '{}', '127.0.0.1', '2018-09-07 10:12:30');
INSERT INTO `sys_log` VALUES ('1037886295875878914', '1', 'admin', '查询在线用户列表数据', '8', 'SessionController.list()', '{\"name\":[\"\"]}', '127.0.0.1', '2018-09-07 10:12:30');
INSERT INTO `sys_log` VALUES ('1037886308190355458', '1', 'admin', '进入数据字典列表页面', '0', 'com.ifast.common.controller.DictController.sysDict()', '{}', '127.0.0.1', '2018-09-07 10:12:33');
INSERT INTO `sys_log` VALUES ('1037886309192794114', '1', 'admin', '查询数据字典列表', '16', 'com.ifast.common.controller.DictController.list()', '{\"pageNumber\":[\"1\"],\"pageSize\":[\"10\"],\"type\":[\"\"]}', '127.0.0.1', '2018-09-07 10:12:33');
INSERT INTO `sys_log` VALUES ('1037886313076719617', '1', 'admin', '进入文件管理页面', '0', 'FileController.sysFile()', '{}', '127.0.0.1', '2018-09-07 10:12:34');
INSERT INTO `sys_log` VALUES ('1037886313814917122', '1', 'admin', '查询文件列表', '32', 'FileController.list()', '{\"type\":[\"\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:34');
INSERT INTO `sys_log` VALUES ('1037886327526096897', '1', 'admin', '查询文件列表', '6', 'FileController.list()', '{\"type\":[\"\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:38');
INSERT INTO `sys_log` VALUES ('1037886331732983810', '1', 'admin', '查询文件列表', '10', 'FileController.list()', '{\"type\":[\"0\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:39');
INSERT INTO `sys_log` VALUES ('1037886367032246274', '1', 'admin', '查询文件列表', '8', 'FileController.list()', '{\"type\":[\"99\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:47');
INSERT INTO `sys_log` VALUES ('1037886371864084481', '1', 'admin', '查询文件列表', '7', 'FileController.list()', '{\"type\":[\"3\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:48');
INSERT INTO `sys_log` VALUES ('1037886409214361601', '1', 'admin', '查询文件列表', '6', 'FileController.list()', '{\"type\":[\"2\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:57');
INSERT INTO `sys_log` VALUES ('1037886413513523202', '1', 'admin', '查询文件列表', '6', 'FileController.list()', '{\"type\":[\"1\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:58');
INSERT INTO `sys_log` VALUES ('1037886415501623298', '1', 'admin', '查询文件列表', '6', 'FileController.list()', '{\"type\":[\"0\"],\"pageNumber\":[\"1\"],\"pageSize\":[\"12\"]}', '127.0.0.1', '2018-09-07 10:12:59');
INSERT INTO `sys_log` VALUES ('1037886510628438017', '1', 'admin', '进入个人中心', '26', 'UserController.personal()', '{}', '127.0.0.1', '2018-09-07 10:13:21');
INSERT INTO `sys_log` VALUES ('1037887171621335042', '1', 'admin', '登录', '35', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:15:59');
INSERT INTO `sys_log` VALUES ('1037887171860410370', '1', 'admin', '请求访问主页', '24', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:15:59');
INSERT INTO `sys_log` VALUES ('1037887172963512321', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:15:59');
INSERT INTO `sys_log` VALUES ('1037887174087585793', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:15:59');
INSERT INTO `sys_log` VALUES ('1037887897487667201', '1', 'admin', '登录', '34', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:18:52');
INSERT INTO `sys_log` VALUES ('1037887897714159617', '1', 'admin', '请求访问主页', '22', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:18:52');
INSERT INTO `sys_log` VALUES ('1037887898628517889', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:18:52');
INSERT INTO `sys_log` VALUES ('1037887899614179329', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:18:52');
INSERT INTO `sys_log` VALUES ('1037889646621487106', '1', 'admin', '登录', '11', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:25:49');
INSERT INTO `sys_log` VALUES ('1037889646822813697', '1', 'admin', '请求访问主页', '17', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:25:49');
INSERT INTO `sys_log` VALUES ('1037889647481319425', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:25:49');
INSERT INTO `sys_log` VALUES ('1037889648500535297', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:25:49');
INSERT INTO `sys_log` VALUES ('1037891529847148545', '1', 'admin', '登录', '38', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:33:18');
INSERT INTO `sys_log` VALUES ('1037891530065252354', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:33:18');
INSERT INTO `sys_log` VALUES ('1037891530929278977', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:33:18');
INSERT INTO `sys_log` VALUES ('1037891532158210049', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:33:19');
INSERT INTO `sys_log` VALUES ('1037892146137182210', '1', 'admin', '登录', '37', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:35:45');
INSERT INTO `sys_log` VALUES ('1037892146393034753', '1', 'admin', '请求访问主页', '28', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:35:45');
INSERT INTO `sys_log` VALUES ('1037892146934099970', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:35:45');
INSERT INTO `sys_log` VALUES ('1037892148481798145', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:35:45');
INSERT INTO `sys_log` VALUES ('1037892395228508162', '1', 'admin', '登录', '22', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:36:44');
INSERT INTO `sys_log` VALUES ('1037892395375308801', '1', 'admin', '请求访问主页', '14', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:36:44');
INSERT INTO `sys_log` VALUES ('1037892395924762625', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:36:44');
INSERT INTO `sys_log` VALUES ('1037892396570685442', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:36:45');
INSERT INTO `sys_log` VALUES ('1037892645146083330', '1', 'admin', '登录', '35', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:37:44');
INSERT INTO `sys_log` VALUES ('1037892645376770050', '1', 'admin', '请求访问主页', '21', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:37:44');
INSERT INTO `sys_log` VALUES ('1037892646152716289', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:37:44');
INSERT INTO `sys_log` VALUES ('1037892647868186625', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:37:45');
INSERT INTO `sys_log` VALUES ('1037893606358999042', '1', 'admin', '登录', '34', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:41:33');
INSERT INTO `sys_log` VALUES ('1037893606572908545', '1', 'admin', '请求访问主页', '22', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:41:33');
INSERT INTO `sys_log` VALUES ('1037893607147528194', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:41:33');
INSERT INTO `sys_log` VALUES ('1037893608481316865', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:41:34');
INSERT INTO `sys_log` VALUES ('1037894230253338625', '1', 'admin', '登录', '35', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:44:02');
INSERT INTO `sys_log` VALUES ('1037894230517579777', '1', 'admin', '请求访问主页', '22', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:44:02');
INSERT INTO `sys_log` VALUES ('1037894231264165889', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:44:02');
INSERT INTO `sys_log` VALUES ('1037894232283381761', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:44:02');
INSERT INTO `sys_log` VALUES ('1037897350303805442', '1', 'admin', '登录', '32', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:56:26');
INSERT INTO `sys_log` VALUES ('1037897350513520642', '1', 'admin', '请求访问主页', '21', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:56:26');
INSERT INTO `sys_log` VALUES ('1037897351360770049', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:56:26');
INSERT INTO `sys_log` VALUES ('1037897352413540354', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:56:26');
INSERT INTO `sys_log` VALUES ('1037897746044743681', '-1', '{\"username\":[\"admin\"],\"password\":[\"1\\\\\"]}', '登录', '28', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\\\\\"]}', '127.0.0.1', '2018-09-07 10:58:00');
INSERT INTO `sys_log` VALUES ('1037897750452957185', '1', 'admin', '登录', '28', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 10:58:01');
INSERT INTO `sys_log` VALUES ('1037897750624923649', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 10:58:01');
INSERT INTO `sys_log` VALUES ('1037897751262457857', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:58:01');
INSERT INTO `sys_log` VALUES ('1037897752654966786', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 10:58:02');
INSERT INTO `sys_log` VALUES ('1037898486737887233', '1', 'admin', '登录', '33', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 11:00:57');
INSERT INTO `sys_log` VALUES ('1037898486947602434', '1', 'admin', '请求访问主页', '23', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 11:00:57');
INSERT INTO `sys_log` VALUES ('1037898487878737922', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:00:57');
INSERT INTO `sys_log` VALUES ('1037898489241886722', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:00:57');
INSERT INTO `sys_log` VALUES ('1037901333722390529', '1', 'admin', '请求访问主页', '9', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 11:12:15');
INSERT INTO `sys_log` VALUES ('1037901334204735489', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:12:15');
INSERT INTO `sys_log` VALUES ('1037901334888407041', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:12:16');
INSERT INTO `sys_log` VALUES ('1037902180292321281', '1', 'admin', '登录', '11', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 11:15:37');
INSERT INTO `sys_log` VALUES ('1037902180405567489', '1', 'admin', '请求访问主页', '11', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 11:15:37');
INSERT INTO `sys_log` VALUES ('1037902180887912449', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:15:37');
INSERT INTO `sys_log` VALUES ('1037902181634498561', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:15:38');
INSERT INTO `sys_log` VALUES ('1037904442175610881', '1', 'admin', '登录', '9', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 11:24:37');
INSERT INTO `sys_log` VALUES ('1037904442314022914', '1', 'admin', '请求访问主页', '15', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 11:24:37');
INSERT INTO `sys_log` VALUES ('1037904442687315969', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:24:37');
INSERT INTO `sys_log` VALUES ('1037904443127717890', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 11:24:37');
INSERT INTO `sys_log` VALUES ('1037953934010703874', '1', 'admin', '登录', '7', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 14:41:16');
INSERT INTO `sys_log` VALUES ('1037953934111367169', '1', 'admin', '请求访问主页', '8', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 14:41:16');
INSERT INTO `sys_log` VALUES ('1037953934476271618', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 14:41:16');
INSERT INTO `sys_log` VALUES ('1037953935604539393', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 14:41:17');
INSERT INTO `sys_log` VALUES ('1037953997302751233', '1', 'admin', '进入系统菜单页面', '0', 'MenuController.menu()', '{}', '127.0.0.1', '2018-09-07 14:41:31');
INSERT INTO `sys_log` VALUES ('1037953998447796226', '1', 'admin', '查询菜单列表', '5', 'MenuController.list()', '{}', '127.0.0.1', '2018-09-07 14:41:32');
INSERT INTO `sys_log` VALUES ('1037954008065335298', '1', 'admin', '编辑菜单', '4', 'MenuController.edit()', '{}', '127.0.0.1', '2018-09-07 14:41:34');
INSERT INTO `sys_log` VALUES ('1037954035085041666', '1', 'admin', '更新菜单', '12', 'MenuController.update()', '{\"parentId\":[\"0\"],\"id\":[\"256\"],\"type\":[\"0\"],\"name\":[\"官网管理\"],\"url\":[\"\"],\"perms\":[\"\"],\"orderNum\":[\"\"],\"icon\":[\"fa fa-newspaper-o\"]}', '127.0.0.1', '2018-09-07 14:41:40');
INSERT INTO `sys_log` VALUES ('1037954035236036610', '1', 'admin', '查询菜单列表', '7', 'MenuController.list()', '{}', '127.0.0.1', '2018-09-07 14:41:40');
INSERT INTO `sys_log` VALUES ('1037954052201996290', '1', 'admin', '请求访问主页', '7', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 14:41:44');
INSERT INTO `sys_log` VALUES ('1037954052487208961', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 14:41:45');
INSERT INTO `sys_log` VALUES ('1037954052935999489', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 14:41:45');
INSERT INTO `sys_log` VALUES ('1037954457694724098', '1', 'admin', '登录', '11', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 14:43:21');
INSERT INTO `sys_log` VALUES ('1037954457778610177', '1', 'admin', '请求访问主页', '13', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 14:43:21');
INSERT INTO `sys_log` VALUES ('1037954458193846274', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 14:43:21');
INSERT INTO `sys_log` VALUES ('1037954459192090626', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 14:43:21');
INSERT INTO `sys_log` VALUES ('1037963388869414914', '-1', '{}', '重定向到登录', '1', 'LoginController.welcome()', '{}', '127.0.0.1', '2018-09-07 15:18:50');
INSERT INTO `sys_log` VALUES ('1037963583346708481', '1', 'admin', '登录', '55', 'LoginController.ajaxLogin()', '{\"username\":[\"admin\"],\"password\":[\"1\"]}', '127.0.0.1', '2018-09-07 15:19:37');
INSERT INTO `sys_log` VALUES ('1037963584030380034', '1', 'admin', '请求访问主页', '21', 'LoginController.index()', '{}', '127.0.0.1', '2018-09-07 15:19:37');
INSERT INTO `sys_log` VALUES ('1037963584881823746', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 15:19:37');
INSERT INTO `sys_log` VALUES ('1037963585972342786', '1', 'admin', '主页', '0', 'LoginController.main()', '{}', '127.0.0.1', '2018-09-07 15:19:37');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parentId` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `orderNum` int(11) DEFAULT NULL COMMENT '排序',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  `gmtModified` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '基础管理', '', '', '0', 'fa fa-bars', '0', '2017-08-09 22:49:47', null);
INSERT INTO `sys_menu` VALUES ('2', '3', '系统菜单', 'sys/menu/', 'sys:menu:menu', '1', 'fa fa-th-list', '2', '2017-08-09 22:55:15', null);
INSERT INTO `sys_menu` VALUES ('3', '0', '系统管理', null, null, '0', 'fa fa-desktop', '1', '2017-08-09 23:06:55', '2017-08-14 14:13:43');
INSERT INTO `sys_menu` VALUES ('6', '3', '用户管理', 'sys/user/', 'sys:user:user', '1', 'fa fa-user', '0', '2017-08-10 14:12:11', null);
INSERT INTO `sys_menu` VALUES ('7', '3', '角色管理', 'sys/role', 'sys:role:role', '1', 'fa fa-paw', '1', '2017-08-10 14:13:19', null);
INSERT INTO `sys_menu` VALUES ('12', '6', '新增', '', 'sys:user:add', '2', '', '0', '2017-08-14 10:51:35', null);
INSERT INTO `sys_menu` VALUES ('13', '6', '编辑', '', 'sys:user:edit', '2', '', '0', '2017-08-14 10:52:06', null);
INSERT INTO `sys_menu` VALUES ('14', '6', '删除', null, 'sys:user:remove', '2', null, '0', '2017-08-14 10:52:24', null);
INSERT INTO `sys_menu` VALUES ('15', '7', '新增', '', 'sys:role:add', '2', '', '0', '2017-08-14 10:56:37', null);
INSERT INTO `sys_menu` VALUES ('20', '2', '新增', '', 'sys:menu:add', '2', '', '0', '2017-08-14 10:59:32', null);
INSERT INTO `sys_menu` VALUES ('21', '2', '编辑', '', 'sys:menu:edit', '2', '', '0', '2017-08-14 10:59:56', null);
INSERT INTO `sys_menu` VALUES ('22', '2', '删除', '', 'sys:menu:remove', '2', '', '0', '2017-08-14 11:00:26', null);
INSERT INTO `sys_menu` VALUES ('24', '6', '批量删除', '', 'sys:user:batchRemove', '2', '', '0', '2017-08-14 17:27:18', null);
INSERT INTO `sys_menu` VALUES ('25', '6', '停用', null, 'sys:user:disable', '2', null, '0', '2017-08-14 17:27:43', null);
INSERT INTO `sys_menu` VALUES ('26', '6', '重置密码', '', 'sys:user:resetPwd', '2', '', '0', '2017-08-14 17:28:34', null);
INSERT INTO `sys_menu` VALUES ('27', '91', '系统日志', 'common/log', 'common:log', '1', 'fa fa-warning', '0', '2017-08-14 22:11:53', null);
INSERT INTO `sys_menu` VALUES ('28', '27', '刷新', null, 'sys:log:list', '2', null, '0', '2017-08-14 22:30:22', null);
INSERT INTO `sys_menu` VALUES ('29', '27', '删除', null, 'sys:log:remove', '2', null, '0', '2017-08-14 22:30:43', null);
INSERT INTO `sys_menu` VALUES ('30', '27', '清空', null, 'sys:log:clear', '2', null, '0', '2017-08-14 22:31:02', null);
INSERT INTO `sys_menu` VALUES ('48', '77', '代码生成', 'common/generator', 'common:generator', '1', 'fa fa-code', '3', null, null);
INSERT INTO `sys_menu` VALUES ('55', '7', '编辑', '', 'sys:role:edit', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('56', '7', '删除', '', 'sys:role:remove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('57', '91', '运行监控', '/druid/index.html', '', '1', 'fa fa-caret-square-o-right', '1', null, null);
INSERT INTO `sys_menu` VALUES ('61', '2', '批量删除', '', 'sys:menu:batchRemove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('62', '7', '批量删除', '', 'sys:role:batchRemove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('71', '1', '文件管理', '/common/sysFile', 'oss:file:file', '1', 'fa fa-folder-open', '2', null, null);
INSERT INTO `sys_menu` VALUES ('72', '77', '计划任务', 'common/job', 'common:taskScheduleJob', '1', 'fa fa-hourglass-1', '4', null, null);
INSERT INTO `sys_menu` VALUES ('73', '3', '部门管理', '/sys/dept', 'system:sysDept:sysDept', '1', 'fa fa-users', '3', null, null);
INSERT INTO `sys_menu` VALUES ('74', '73', '增加', '/sys/dept/add', 'system:sysDept:add', '2', null, '1', null, null);
INSERT INTO `sys_menu` VALUES ('75', '73', '刪除', 'sys/dept/remove', 'system:sysDept:remove', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('76', '73', '编辑', '/sys/dept/edit', 'system:sysDept:edit', '2', null, '3', null, null);
INSERT INTO `sys_menu` VALUES ('77', '0', '系统工具', '', '', '0', 'fa fa-gear', '4', null, null);
INSERT INTO `sys_menu` VALUES ('78', '1', '数据字典', '/common/sysDict', 'common:sysDict:sysDict', '1', 'fa fa-book', '1', null, null);
INSERT INTO `sys_menu` VALUES ('79', '78', '增加', '/common/sysDict/add', 'common:sysDict:add', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('80', '78', '编辑', '/common/sysDict/edit', 'common:sysDict:edit', '2', null, '2', null, null);
INSERT INTO `sys_menu` VALUES ('81', '78', '删除', '/common/sysDict/remove', 'common:sysDict:remove', '2', '', '3', null, null);
INSERT INTO `sys_menu` VALUES ('83', '78', '批量删除', '/common/sysDict/batchRemove', 'common:sysDict:batchRemove', '2', '', '4', null, null);
INSERT INTO `sys_menu` VALUES ('91', '0', '系统监控', '', '', '0', 'fa fa-video-camera', '5', null, null);
INSERT INTO `sys_menu` VALUES ('92', '91', '在线用户', 'sys/online', '', '1', 'fa fa-user', null, null, null);
INSERT INTO `sys_menu` VALUES ('97', '0', '图表管理', '', '', '0', 'fa fa-bar-chart', '7', null, null);
INSERT INTO `sys_menu` VALUES ('98', '97', '百度chart', '/chart/graph_echarts.html', '', '1', 'fa fa-area-chart', null, null, null);
INSERT INTO `sys_menu` VALUES ('175', '1', '系统配置', '/common/config', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('176', '175', '查看', null, 'common:config:config', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('177', '175', '新增', null, 'common:config:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('178', '175', '修改', null, 'common:config:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('179', '175', '删除', null, 'common:config:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('180', '175', '批量删除', null, 'common:config:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('181', '199', '公众号管理', '/wxmp/mpConfig', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('182', '181', '查看', null, 'wxmp:mpConfig:mpConfig', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('183', '181', '新增', null, 'wxmp:mpConfig:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('184', '181', '修改', null, 'wxmp:mpConfig:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('185', '181', '删除', null, 'wxmp:mpConfig:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('186', '181', '批量删除', null, 'wxmp:mpConfig:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('187', '199', '微信粉丝表', '/wxmp/mpFans', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('188', '187', '查看', null, 'wxmp:mpFans:mpFans', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('189', '187', '新增', null, 'wxmp:mpFans:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('190', '187', '修改', null, 'wxmp:mpFans:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('191', '187', '删除', null, 'wxmp:mpFans:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('192', '187', '批量删除', null, 'wxmp:mpFans:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('193', '71', '增加', '/common/sysFile/add', 'oss:file:add', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('194', '71', '列表', '/common/sysFile/list', 'oss:file:list', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('195', '71', '编辑', '/common/sysFile/edit', 'oss:file:update', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('196', '71', '查询', '/common/sysFile/info', 'oss:file:info', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('197', '71', '删除', '/common/sysFile/remove', 'oss:file:remove', '2', null, null, null, null);
INSERT INTO `sys_menu` VALUES ('199', '0', '微信公众号', null, null, '0', 'fa fa-file-code-o', '3', null, null);
INSERT INTO `sys_menu` VALUES ('205', '187', '同步', null, 'wxmp:mpFans:sync', '2', 'fa fa-refresh', '5', null, null);
INSERT INTO `sys_menu` VALUES ('206', '1', '[Demo]基础表', '/demo/demoBase', '', '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('207', '206', '查看', null, 'demo:demoBase:demoBase', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('208', '206', '新增', null, 'demo:demoBase:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('209', '206', '修改', null, 'demo:demoBase:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('210', '206', '删除', null, 'demo:demoBase:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('211', '206', '批量删除', null, 'demo:demoBase:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('212', '0', 'api测试-用户更新', '', 'api:user:update', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('213', '0', 'api测试-appUser角色', '', 'api:user:role', '2', '', null, null, null);
INSERT INTO `sys_menu` VALUES ('214', '256', '文章表', '/cms/article', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('215', '214', '查看', null, 'ifast:article:article', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('216', '214', '新增', null, 'ifast:article:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('217', '214', '修改', null, 'ifast:article:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('218', '214', '删除', null, 'ifast:article:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('219', '214', '批量删除', null, 'ifast:article:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('220', '256', '文章详表', '/cms/articleData', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('221', '220', '查看', null, 'ifast:articleData:articleData', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('222', '220', '新增', null, 'ifast:articleData:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('223', '220', '修改', null, 'ifast:articleData:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('224', '220', '删除', null, 'ifast:articleData:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('225', '220', '批量删除', null, 'ifast:articleData:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('226', '256', '栏目表', '/cms/category', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('227', '226', '查看', null, 'ifast:category:category', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('228', '226', '新增', null, 'ifast:category:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('229', '226', '修改', null, 'ifast:category:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('230', '226', '删除', null, 'ifast:category:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('231', '226', '批量删除', null, 'ifast:category:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('232', '256', '评论表', '/cms/comment', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('233', '232', '查看', null, 'ifast:comment:comment', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('234', '232', '新增', null, 'ifast:comment:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('235', '232', '修改', null, 'ifast:comment:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('236', '232', '删除', null, 'ifast:comment:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('237', '232', '批量删除', null, 'ifast:comment:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('238', '256', '留言板', '/cms/guestbook', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('239', '238', '查看', null, 'ifast:guestbook:guestbook', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('240', '238', '新增', null, 'ifast:guestbook:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('241', '238', '修改', null, 'ifast:guestbook:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('242', '238', '删除', null, 'ifast:guestbook:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('243', '238', '批量删除', null, 'ifast:guestbook:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('244', '256', '友情链接', '/cms/link', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('245', '244', '查看', null, 'ifast:link:link', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('246', '244', '新增', null, 'ifast:link:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('247', '244', '修改', null, 'ifast:link:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('248', '244', '删除', null, 'ifast:link:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('249', '244', '批量删除', null, 'ifast:link:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('250', '256', '站点表', '/cms/site', null, '1', 'fa fa-file-code-o', '6', null, null);
INSERT INTO `sys_menu` VALUES ('251', '250', '查看', null, 'ifast:site:site', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('252', '250', '新增', null, 'ifast:site:add', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('253', '250', '修改', null, 'ifast:site:edit', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('254', '250', '删除', null, 'ifast:site:remove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('255', '250', '批量删除', null, 'ifast:site:batchRemove', '2', null, '6', null, null);
INSERT INTO `sys_menu` VALUES ('256', '0', '官网管理', '', '', '0', 'fa fa-newspaper-o', null, null, null);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `roleSign` varchar(100) DEFAULT NULL COMMENT '角色标识',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `userIdCreate` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  `gmtModified` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级用户角色', 'adminRole', '超级管理员', '2', '2017-08-12 00:43:52', '2017-08-12 19:14:59');
INSERT INTO `sys_role` VALUES ('2', '普通用户', null, '普通用户', null, null, null);
INSERT INTO `sys_role` VALUES ('3', '前端API', 'apiRole', '前端API', null, null, null);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roleId` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menuId` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4647 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('367', '44', '1');
INSERT INTO `sys_role_menu` VALUES ('368', '44', '32');
INSERT INTO `sys_role_menu` VALUES ('369', '44', '33');
INSERT INTO `sys_role_menu` VALUES ('370', '44', '34');
INSERT INTO `sys_role_menu` VALUES ('371', '44', '35');
INSERT INTO `sys_role_menu` VALUES ('372', '44', '28');
INSERT INTO `sys_role_menu` VALUES ('373', '44', '29');
INSERT INTO `sys_role_menu` VALUES ('374', '44', '30');
INSERT INTO `sys_role_menu` VALUES ('375', '44', '38');
INSERT INTO `sys_role_menu` VALUES ('376', '44', '4');
INSERT INTO `sys_role_menu` VALUES ('377', '44', '27');
INSERT INTO `sys_role_menu` VALUES ('378', '45', '38');
INSERT INTO `sys_role_menu` VALUES ('379', '46', '3');
INSERT INTO `sys_role_menu` VALUES ('380', '46', '20');
INSERT INTO `sys_role_menu` VALUES ('381', '46', '21');
INSERT INTO `sys_role_menu` VALUES ('382', '46', '22');
INSERT INTO `sys_role_menu` VALUES ('383', '46', '23');
INSERT INTO `sys_role_menu` VALUES ('384', '46', '11');
INSERT INTO `sys_role_menu` VALUES ('385', '46', '12');
INSERT INTO `sys_role_menu` VALUES ('386', '46', '13');
INSERT INTO `sys_role_menu` VALUES ('387', '46', '14');
INSERT INTO `sys_role_menu` VALUES ('388', '46', '24');
INSERT INTO `sys_role_menu` VALUES ('389', '46', '25');
INSERT INTO `sys_role_menu` VALUES ('390', '46', '26');
INSERT INTO `sys_role_menu` VALUES ('391', '46', '15');
INSERT INTO `sys_role_menu` VALUES ('392', '46', '2');
INSERT INTO `sys_role_menu` VALUES ('393', '46', '6');
INSERT INTO `sys_role_menu` VALUES ('394', '46', '7');
INSERT INTO `sys_role_menu` VALUES ('598', '50', '38');
INSERT INTO `sys_role_menu` VALUES ('632', '38', '42');
INSERT INTO `sys_role_menu` VALUES ('737', '51', '38');
INSERT INTO `sys_role_menu` VALUES ('738', '51', '39');
INSERT INTO `sys_role_menu` VALUES ('739', '51', '40');
INSERT INTO `sys_role_menu` VALUES ('740', '51', '41');
INSERT INTO `sys_role_menu` VALUES ('741', '51', '4');
INSERT INTO `sys_role_menu` VALUES ('742', '51', '32');
INSERT INTO `sys_role_menu` VALUES ('743', '51', '33');
INSERT INTO `sys_role_menu` VALUES ('744', '51', '34');
INSERT INTO `sys_role_menu` VALUES ('745', '51', '35');
INSERT INTO `sys_role_menu` VALUES ('746', '51', '27');
INSERT INTO `sys_role_menu` VALUES ('747', '51', '28');
INSERT INTO `sys_role_menu` VALUES ('748', '51', '29');
INSERT INTO `sys_role_menu` VALUES ('749', '51', '30');
INSERT INTO `sys_role_menu` VALUES ('750', '51', '1');
INSERT INTO `sys_role_menu` VALUES ('1064', '54', '53');
INSERT INTO `sys_role_menu` VALUES ('1095', '55', '2');
INSERT INTO `sys_role_menu` VALUES ('1096', '55', '6');
INSERT INTO `sys_role_menu` VALUES ('1097', '55', '7');
INSERT INTO `sys_role_menu` VALUES ('1098', '55', '3');
INSERT INTO `sys_role_menu` VALUES ('1099', '55', '50');
INSERT INTO `sys_role_menu` VALUES ('1100', '55', '49');
INSERT INTO `sys_role_menu` VALUES ('1101', '55', '1');
INSERT INTO `sys_role_menu` VALUES ('1856', '53', '28');
INSERT INTO `sys_role_menu` VALUES ('1857', '53', '29');
INSERT INTO `sys_role_menu` VALUES ('1858', '53', '30');
INSERT INTO `sys_role_menu` VALUES ('1859', '53', '27');
INSERT INTO `sys_role_menu` VALUES ('1860', '53', '57');
INSERT INTO `sys_role_menu` VALUES ('1861', '53', '71');
INSERT INTO `sys_role_menu` VALUES ('1862', '53', '48');
INSERT INTO `sys_role_menu` VALUES ('1863', '53', '72');
INSERT INTO `sys_role_menu` VALUES ('1864', '53', '1');
INSERT INTO `sys_role_menu` VALUES ('1865', '53', '7');
INSERT INTO `sys_role_menu` VALUES ('1866', '53', '55');
INSERT INTO `sys_role_menu` VALUES ('1867', '53', '56');
INSERT INTO `sys_role_menu` VALUES ('1868', '53', '62');
INSERT INTO `sys_role_menu` VALUES ('1869', '53', '15');
INSERT INTO `sys_role_menu` VALUES ('1870', '53', '2');
INSERT INTO `sys_role_menu` VALUES ('1871', '53', '61');
INSERT INTO `sys_role_menu` VALUES ('1872', '53', '20');
INSERT INTO `sys_role_menu` VALUES ('1873', '53', '21');
INSERT INTO `sys_role_menu` VALUES ('1874', '53', '22');
INSERT INTO `sys_role_menu` VALUES ('2247', '63', '-1');
INSERT INTO `sys_role_menu` VALUES ('2248', '63', '84');
INSERT INTO `sys_role_menu` VALUES ('2249', '63', '85');
INSERT INTO `sys_role_menu` VALUES ('2250', '63', '88');
INSERT INTO `sys_role_menu` VALUES ('2251', '63', '87');
INSERT INTO `sys_role_menu` VALUES ('2252', '64', '84');
INSERT INTO `sys_role_menu` VALUES ('2253', '64', '89');
INSERT INTO `sys_role_menu` VALUES ('2254', '64', '88');
INSERT INTO `sys_role_menu` VALUES ('2255', '64', '87');
INSERT INTO `sys_role_menu` VALUES ('2256', '64', '86');
INSERT INTO `sys_role_menu` VALUES ('2257', '64', '85');
INSERT INTO `sys_role_menu` VALUES ('2258', '65', '89');
INSERT INTO `sys_role_menu` VALUES ('2259', '65', '88');
INSERT INTO `sys_role_menu` VALUES ('2260', '65', '86');
INSERT INTO `sys_role_menu` VALUES ('2262', '67', '48');
INSERT INTO `sys_role_menu` VALUES ('2263', '68', '88');
INSERT INTO `sys_role_menu` VALUES ('2264', '68', '87');
INSERT INTO `sys_role_menu` VALUES ('2265', '69', '89');
INSERT INTO `sys_role_menu` VALUES ('2266', '69', '88');
INSERT INTO `sys_role_menu` VALUES ('2267', '69', '86');
INSERT INTO `sys_role_menu` VALUES ('2268', '69', '87');
INSERT INTO `sys_role_menu` VALUES ('2269', '69', '85');
INSERT INTO `sys_role_menu` VALUES ('2270', '69', '84');
INSERT INTO `sys_role_menu` VALUES ('2271', '70', '85');
INSERT INTO `sys_role_menu` VALUES ('2272', '70', '89');
INSERT INTO `sys_role_menu` VALUES ('2273', '70', '88');
INSERT INTO `sys_role_menu` VALUES ('2274', '70', '87');
INSERT INTO `sys_role_menu` VALUES ('2275', '70', '86');
INSERT INTO `sys_role_menu` VALUES ('2276', '70', '84');
INSERT INTO `sys_role_menu` VALUES ('2277', '71', '87');
INSERT INTO `sys_role_menu` VALUES ('2278', '72', '59');
INSERT INTO `sys_role_menu` VALUES ('2279', '73', '48');
INSERT INTO `sys_role_menu` VALUES ('2280', '74', '88');
INSERT INTO `sys_role_menu` VALUES ('2281', '74', '87');
INSERT INTO `sys_role_menu` VALUES ('2282', '75', '88');
INSERT INTO `sys_role_menu` VALUES ('2283', '75', '87');
INSERT INTO `sys_role_menu` VALUES ('2284', '76', '85');
INSERT INTO `sys_role_menu` VALUES ('2285', '76', '89');
INSERT INTO `sys_role_menu` VALUES ('2286', '76', '88');
INSERT INTO `sys_role_menu` VALUES ('2287', '76', '87');
INSERT INTO `sys_role_menu` VALUES ('2288', '76', '86');
INSERT INTO `sys_role_menu` VALUES ('2289', '76', '84');
INSERT INTO `sys_role_menu` VALUES ('2292', '78', '88');
INSERT INTO `sys_role_menu` VALUES ('2293', '78', '87');
INSERT INTO `sys_role_menu` VALUES ('2294', '78', null);
INSERT INTO `sys_role_menu` VALUES ('2295', '78', null);
INSERT INTO `sys_role_menu` VALUES ('2296', '78', null);
INSERT INTO `sys_role_menu` VALUES ('2308', '80', '87');
INSERT INTO `sys_role_menu` VALUES ('2309', '80', '86');
INSERT INTO `sys_role_menu` VALUES ('2310', '80', '-1');
INSERT INTO `sys_role_menu` VALUES ('2311', '80', '84');
INSERT INTO `sys_role_menu` VALUES ('2312', '80', '85');
INSERT INTO `sys_role_menu` VALUES ('2328', '79', '72');
INSERT INTO `sys_role_menu` VALUES ('2329', '79', '48');
INSERT INTO `sys_role_menu` VALUES ('2330', '79', '77');
INSERT INTO `sys_role_menu` VALUES ('2331', '79', '84');
INSERT INTO `sys_role_menu` VALUES ('2332', '79', '89');
INSERT INTO `sys_role_menu` VALUES ('2333', '79', '88');
INSERT INTO `sys_role_menu` VALUES ('2334', '79', '87');
INSERT INTO `sys_role_menu` VALUES ('2335', '79', '86');
INSERT INTO `sys_role_menu` VALUES ('2336', '79', '85');
INSERT INTO `sys_role_menu` VALUES ('2337', '79', '-1');
INSERT INTO `sys_role_menu` VALUES ('2338', '77', '89');
INSERT INTO `sys_role_menu` VALUES ('2339', '77', '88');
INSERT INTO `sys_role_menu` VALUES ('2340', '77', '87');
INSERT INTO `sys_role_menu` VALUES ('2341', '77', '86');
INSERT INTO `sys_role_menu` VALUES ('2342', '77', '85');
INSERT INTO `sys_role_menu` VALUES ('2343', '77', '84');
INSERT INTO `sys_role_menu` VALUES ('2344', '77', '72');
INSERT INTO `sys_role_menu` VALUES ('2345', '77', '-1');
INSERT INTO `sys_role_menu` VALUES ('2346', '77', '77');
INSERT INTO `sys_role_menu` VALUES ('3178', '56', '68');
INSERT INTO `sys_role_menu` VALUES ('3179', '56', '60');
INSERT INTO `sys_role_menu` VALUES ('3180', '56', '59');
INSERT INTO `sys_role_menu` VALUES ('3181', '56', '58');
INSERT INTO `sys_role_menu` VALUES ('3182', '56', '51');
INSERT INTO `sys_role_menu` VALUES ('3183', '56', '50');
INSERT INTO `sys_role_menu` VALUES ('3184', '56', '49');
INSERT INTO `sys_role_menu` VALUES ('3185', '56', '-1');
INSERT INTO `sys_role_menu` VALUES ('4544', '1034088931742957569', '1034089959238385666');
INSERT INTO `sys_role_menu` VALUES ('4545', '1034088931742957569', '1034090238251876354');
INSERT INTO `sys_role_menu` VALUES ('4546', '1034088931742957569', '-1');
INSERT INTO `sys_role_menu` VALUES ('4547', '1', '193');
INSERT INTO `sys_role_menu` VALUES ('4548', '1', '194');
INSERT INTO `sys_role_menu` VALUES ('4549', '1', '195');
INSERT INTO `sys_role_menu` VALUES ('4550', '1', '196');
INSERT INTO `sys_role_menu` VALUES ('4551', '1', '197');
INSERT INTO `sys_role_menu` VALUES ('4552', '1', '79');
INSERT INTO `sys_role_menu` VALUES ('4553', '1', '80');
INSERT INTO `sys_role_menu` VALUES ('4554', '1', '81');
INSERT INTO `sys_role_menu` VALUES ('4555', '1', '83');
INSERT INTO `sys_role_menu` VALUES ('4556', '1', '176');
INSERT INTO `sys_role_menu` VALUES ('4557', '1', '177');
INSERT INTO `sys_role_menu` VALUES ('4558', '1', '178');
INSERT INTO `sys_role_menu` VALUES ('4559', '1', '179');
INSERT INTO `sys_role_menu` VALUES ('4560', '1', '180');
INSERT INTO `sys_role_menu` VALUES ('4561', '1', '207');
INSERT INTO `sys_role_menu` VALUES ('4562', '1', '208');
INSERT INTO `sys_role_menu` VALUES ('4563', '1', '209');
INSERT INTO `sys_role_menu` VALUES ('4564', '1', '210');
INSERT INTO `sys_role_menu` VALUES ('4565', '1', '211');
INSERT INTO `sys_role_menu` VALUES ('4566', '1', '20');
INSERT INTO `sys_role_menu` VALUES ('4567', '1', '21');
INSERT INTO `sys_role_menu` VALUES ('4568', '1', '22');
INSERT INTO `sys_role_menu` VALUES ('4569', '1', '61');
INSERT INTO `sys_role_menu` VALUES ('4570', '1', '12');
INSERT INTO `sys_role_menu` VALUES ('4571', '1', '13');
INSERT INTO `sys_role_menu` VALUES ('4572', '1', '14');
INSERT INTO `sys_role_menu` VALUES ('4573', '1', '24');
INSERT INTO `sys_role_menu` VALUES ('4574', '1', '25');
INSERT INTO `sys_role_menu` VALUES ('4575', '1', '26');
INSERT INTO `sys_role_menu` VALUES ('4576', '1', '15');
INSERT INTO `sys_role_menu` VALUES ('4577', '1', '55');
INSERT INTO `sys_role_menu` VALUES ('4578', '1', '56');
INSERT INTO `sys_role_menu` VALUES ('4579', '1', '62');
INSERT INTO `sys_role_menu` VALUES ('4580', '1', '74');
INSERT INTO `sys_role_menu` VALUES ('4581', '1', '75');
INSERT INTO `sys_role_menu` VALUES ('4582', '1', '76');
INSERT INTO `sys_role_menu` VALUES ('4583', '1', '48');
INSERT INTO `sys_role_menu` VALUES ('4584', '1', '72');
INSERT INTO `sys_role_menu` VALUES ('4585', '1', '28');
INSERT INTO `sys_role_menu` VALUES ('4586', '1', '29');
INSERT INTO `sys_role_menu` VALUES ('4587', '1', '30');
INSERT INTO `sys_role_menu` VALUES ('4588', '1', '57');
INSERT INTO `sys_role_menu` VALUES ('4589', '1', '92');
INSERT INTO `sys_role_menu` VALUES ('4590', '1', '71');
INSERT INTO `sys_role_menu` VALUES ('4591', '1', '78');
INSERT INTO `sys_role_menu` VALUES ('4592', '1', '175');
INSERT INTO `sys_role_menu` VALUES ('4593', '1', '206');
INSERT INTO `sys_role_menu` VALUES ('4594', '1', '1');
INSERT INTO `sys_role_menu` VALUES ('4595', '1', '2');
INSERT INTO `sys_role_menu` VALUES ('4596', '1', '6');
INSERT INTO `sys_role_menu` VALUES ('4597', '1', '7');
INSERT INTO `sys_role_menu` VALUES ('4598', '1', '73');
INSERT INTO `sys_role_menu` VALUES ('4599', '1', '3');
INSERT INTO `sys_role_menu` VALUES ('4600', '1', '77');
INSERT INTO `sys_role_menu` VALUES ('4601', '1', '27');
INSERT INTO `sys_role_menu` VALUES ('4602', '1', '91');
INSERT INTO `sys_role_menu` VALUES ('4603', '1', '256');
INSERT INTO `sys_role_menu` VALUES ('4604', '1', '215');
INSERT INTO `sys_role_menu` VALUES ('4605', '1', '216');
INSERT INTO `sys_role_menu` VALUES ('4606', '1', '217');
INSERT INTO `sys_role_menu` VALUES ('4607', '1', '218');
INSERT INTO `sys_role_menu` VALUES ('4608', '1', '219');
INSERT INTO `sys_role_menu` VALUES ('4609', '1', '221');
INSERT INTO `sys_role_menu` VALUES ('4610', '1', '222');
INSERT INTO `sys_role_menu` VALUES ('4611', '1', '223');
INSERT INTO `sys_role_menu` VALUES ('4612', '1', '224');
INSERT INTO `sys_role_menu` VALUES ('4613', '1', '225');
INSERT INTO `sys_role_menu` VALUES ('4614', '1', '227');
INSERT INTO `sys_role_menu` VALUES ('4615', '1', '228');
INSERT INTO `sys_role_menu` VALUES ('4616', '1', '229');
INSERT INTO `sys_role_menu` VALUES ('4617', '1', '230');
INSERT INTO `sys_role_menu` VALUES ('4618', '1', '231');
INSERT INTO `sys_role_menu` VALUES ('4619', '1', '233');
INSERT INTO `sys_role_menu` VALUES ('4620', '1', '234');
INSERT INTO `sys_role_menu` VALUES ('4621', '1', '235');
INSERT INTO `sys_role_menu` VALUES ('4622', '1', '236');
INSERT INTO `sys_role_menu` VALUES ('4623', '1', '237');
INSERT INTO `sys_role_menu` VALUES ('4624', '1', '239');
INSERT INTO `sys_role_menu` VALUES ('4625', '1', '240');
INSERT INTO `sys_role_menu` VALUES ('4626', '1', '241');
INSERT INTO `sys_role_menu` VALUES ('4627', '1', '242');
INSERT INTO `sys_role_menu` VALUES ('4628', '1', '243');
INSERT INTO `sys_role_menu` VALUES ('4629', '1', '245');
INSERT INTO `sys_role_menu` VALUES ('4630', '1', '246');
INSERT INTO `sys_role_menu` VALUES ('4631', '1', '247');
INSERT INTO `sys_role_menu` VALUES ('4632', '1', '248');
INSERT INTO `sys_role_menu` VALUES ('4633', '1', '249');
INSERT INTO `sys_role_menu` VALUES ('4634', '1', '251');
INSERT INTO `sys_role_menu` VALUES ('4635', '1', '252');
INSERT INTO `sys_role_menu` VALUES ('4636', '1', '253');
INSERT INTO `sys_role_menu` VALUES ('4637', '1', '254');
INSERT INTO `sys_role_menu` VALUES ('4638', '1', '255');
INSERT INTO `sys_role_menu` VALUES ('4639', '1', '214');
INSERT INTO `sys_role_menu` VALUES ('4640', '1', '220');
INSERT INTO `sys_role_menu` VALUES ('4641', '1', '226');
INSERT INTO `sys_role_menu` VALUES ('4642', '1', '232');
INSERT INTO `sys_role_menu` VALUES ('4643', '1', '238');
INSERT INTO `sys_role_menu` VALUES ('4644', '1', '244');
INSERT INTO `sys_role_menu` VALUES ('4645', '1', '250');
INSERT INTO `sys_role_menu` VALUES ('4646', '1', '-1');

-- ----------------------------
-- Table structure for sys_task
-- ----------------------------
DROP TABLE IF EXISTS `sys_task`;
CREATE TABLE `sys_task` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cronExpression` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `methodName` varchar(255) DEFAULT NULL COMMENT '任务调用的方法名',
  `isConcurrent` varchar(255) DEFAULT NULL COMMENT '任务是否有状态',
  `description` varchar(255) DEFAULT NULL COMMENT '任务描述',
  `updateBy` varchar(64) DEFAULT NULL COMMENT '更新者',
  `beanClass` varchar(255) DEFAULT NULL COMMENT '任务执行时调用哪个类的方法 包名+类名',
  `createDate` datetime DEFAULT NULL COMMENT '创建时间',
  `jobStatus` varchar(255) DEFAULT NULL COMMENT '任务状态',
  `jobGroup` varchar(255) DEFAULT NULL COMMENT '任务分组',
  `updateDate` datetime DEFAULT NULL COMMENT '更新时间',
  `createBy` varchar(64) DEFAULT NULL COMMENT '创建者',
  `springBean` varchar(255) DEFAULT NULL COMMENT 'Spring bean',
  `jobName` varchar(255) DEFAULT NULL COMMENT '任务名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_task
-- ----------------------------
INSERT INTO `sys_task` VALUES ('2', '0/10 * * * * ?', 'run1', '1', '', '4028ea815a3d2a8c015a3d2f8d2a0002', 'com.ifast.job.TestJob', '2017-05-19 18:30:56', '0', 'group1', '2017-05-19 18:31:07', null, '', 'TestJob');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `deptId` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(255) DEFAULT NULL COMMENT '状态 0:禁用，1:正常',
  `userIdCreate` bigint(255) DEFAULT NULL COMMENT '创建用户id',
  `gmtCreate` datetime DEFAULT NULL COMMENT '创建时间',
  `gmtModified` datetime DEFAULT NULL COMMENT '修改时间',
  `sex` bigint(32) DEFAULT NULL COMMENT '性别',
  `birth` datetime DEFAULT NULL COMMENT '出身日期',
  `picId` bigint(32) DEFAULT NULL,
  `liveAddress` varchar(500) DEFAULT NULL COMMENT '现居住地',
  `hobby` varchar(255) DEFAULT NULL COMMENT '爱好',
  `province` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '所在城市',
  `district` varchar(255) DEFAULT NULL COMMENT '所在地区',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '超级管理员', '33808479d49ca8a3cdc93d4f976d1e3d', '6', 'zhongzyong@163.com', '18620512233', '1', '1', '2017-08-15 21:40:39', '2017-08-15 21:41:00', '96', '2018-04-02 00:00:00', '151', '广州市天河区凌塘村', '', '广东省', '广州市', '天河区');
INSERT INTO `sys_user` VALUES ('2', 'test', '临时用户', 'b132f5f968c9373261f74025c23c2222', '6', 'test@ifast.com', '15278792752', '1', '1', '2017-08-14 13:43:05', '2017-08-14 21:15:36', '96', '2018-08-22 00:00:00', null, '', '', '北京市', '北京市市辖区', '东城区');
INSERT INTO `sys_user` VALUES ('3', 'appUser', 'user', 'fc4d8bf7d69f03344a58f9381dd75dfe', '12', 'appUser@ifast.com', null, '1', null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `roleId` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('73', '30', '48');
INSERT INTO `sys_user_role` VALUES ('74', '30', '49');
INSERT INTO `sys_user_role` VALUES ('75', '30', '50');
INSERT INTO `sys_user_role` VALUES ('76', '31', '48');
INSERT INTO `sys_user_role` VALUES ('77', '31', '49');
INSERT INTO `sys_user_role` VALUES ('78', '31', '52');
INSERT INTO `sys_user_role` VALUES ('79', '32', '48');
INSERT INTO `sys_user_role` VALUES ('80', '32', '49');
INSERT INTO `sys_user_role` VALUES ('81', '32', '50');
INSERT INTO `sys_user_role` VALUES ('82', '32', '51');
INSERT INTO `sys_user_role` VALUES ('83', '32', '52');
INSERT INTO `sys_user_role` VALUES ('84', '33', '38');
INSERT INTO `sys_user_role` VALUES ('85', '33', '49');
INSERT INTO `sys_user_role` VALUES ('86', '33', '52');
INSERT INTO `sys_user_role` VALUES ('87', '34', '50');
INSERT INTO `sys_user_role` VALUES ('88', '34', '51');
INSERT INTO `sys_user_role` VALUES ('89', '34', '52');
INSERT INTO `sys_user_role` VALUES ('110', '1', '1');
INSERT INTO `sys_user_role` VALUES ('111', '2', '1');
INSERT INTO `sys_user_role` VALUES ('117', '135', '1');
INSERT INTO `sys_user_role` VALUES ('120', '134', '1');
INSERT INTO `sys_user_role` VALUES ('121', '134', '48');
INSERT INTO `sys_user_role` VALUES ('124', null, '48');
INSERT INTO `sys_user_role` VALUES ('127', null, '1');
INSERT INTO `sys_user_role` VALUES ('128', null, '1');
INSERT INTO `sys_user_role` VALUES ('129', null, '1');
INSERT INTO `sys_user_role` VALUES ('131', '137', '57');
INSERT INTO `sys_user_role` VALUES ('133', '3', '3');

-- ----------------------------
-- Table structure for wx_mp_config
-- ----------------------------
DROP TABLE IF EXISTS `wx_mp_config`;
CREATE TABLE `wx_mp_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `token` varchar(120) DEFAULT NULL,
  `appId` varchar(64) NOT NULL COMMENT 'APPID',
  `appSecret` varchar(128) NOT NULL COMMENT 'AppSecret',
  `msgMode` int(11) DEFAULT NULL COMMENT '1加密 0不加密',
  `msgSecret` varchar(128) DEFAULT NULL,
  `mpName` varchar(250) DEFAULT NULL COMMENT '公众号名字',
  `appType` int(11) NOT NULL COMMENT '公众号类型： 1.订阅号 2.服务号 3.企业号 4.小程序 5. 测试号',
  `isAuth` int(11) DEFAULT NULL COMMENT '认证授权：1已认证 0未认证',
  `subscribeUrl` varchar(500) DEFAULT NULL COMMENT '提示订阅URL',
  `appKey` varchar(100) DEFAULT NULL COMMENT '开放的公众号key',
  `logo` varchar(255) DEFAULT NULL,
  `createTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='微信配置表';

-- ----------------------------
-- Records of wx_mp_config
-- ----------------------------
INSERT INTO `wx_mp_config` VALUES ('11', '72597b9628704ab09e8b9e8cbe9b540a', 'wxeb5638d307d3df71', '03b1501c72a91f2935037336e43e67e6', '0', '', '源码在线-测试', '5', '0', 'http://xxx.com/test', 'ymhTest', 'http://p6r7ke2jc.bkt.clouddn.com/ifast/20180822/photo_s-1534922328124.png', '2017-11-03 17:32:53');

-- ----------------------------
-- Table structure for wx_mp_fans
-- ----------------------------
DROP TABLE IF EXISTS `wx_mp_fans`;
CREATE TABLE `wx_mp_fans` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mpId` bigint(11) NOT NULL COMMENT '公众号id',
  `openid` varchar(100) DEFAULT NULL COMMENT '用户的标识，对当前公众号唯一',
  `nickname` varchar(100) DEFAULT NULL COMMENT '昵称',
  `subscribe` tinyint(4) DEFAULT NULL COMMENT '用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。0未关注，1已关注',
  `subscribeTime` datetime DEFAULT NULL COMMENT '用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间',
  `subscribeKey` varchar(100) DEFAULT NULL COMMENT '关注来源',
  `sex` tinyint(4) DEFAULT NULL COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `province` varchar(100) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL COMMENT '语言',
  `headimgurl` varchar(500) DEFAULT NULL COMMENT '用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。',
  `unionid` varchar(125) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL COMMENT '公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注',
  `groupid` int(20) DEFAULT NULL COMMENT '分组ID',
  `status` tinyint(11) DEFAULT NULL COMMENT '用户状态 1:正常，0：禁用',
  `tagidList` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2905 DEFAULT CHARSET=utf8 COMMENT='微信粉丝表';

-- ----------------------------
-- Records of wx_mp_fans
-- ----------------------------
INSERT INTO `wx_mp_fans` VALUES ('2899', '5', 'oorK7v4LWw3GMbwt_Ck6PuQ_8_D8', '你是我的眼', '0', null, null, '2', '广州', '中国', '广东', 'zh_CN', 'http://wx.qlogo.cn/mmopen/A7sq8BD8oewx50myY72SwetEVkBXbXDvT5jj6ytorRxqyGwtBu1XTnWGohGXhdTtTwh6VSAbUEUSWpibddJDChg/0', 'oI55m1o8VxrTFvV57WkzEFTHLRIM', null, null, '0', null);
INSERT INTO `wx_mp_fans` VALUES ('2902', '7', 'otO0P09bRa-YRLNlPbJEL1bdDjkQ', 'Aron', '1', '2017-11-24 11:14:28', '', '1', '广州', '中国', '广东', 'zh_CN', 'http://wx.qlogo.cn/mmopen/BQD9yxMcK6CicIrF6tU8Pqucb2VgYicn1iaTMTwm9war1KLT9RlibbsJ9cYal7yypERODjt6XGXC4dVJdVs9woJZBHwI0ibmaGQxY/0', 'oVzGa0kCIhSXljL9wDALjN00ylOs', '', '0', '0', null);
INSERT INTO `wx_mp_fans` VALUES ('2904', '9', 'ozbGr0vZhCS8Pe1lpTuy1tq9Wlls', 'SuSu', '0', '2017-11-26 21:03:25', '', '1', '江北', '中国', '重庆', 'zh_CN', 'http://wx.qlogo.cn/mmopen/8o7KgbIM6ibFyF3coD1mMMdm91kic6Tb68P0hq9lDccec0TllUm5MnBa4WEesfiaW1HUXWqNqCTNUsrYM5iceq9gnesbSPSaE0Yw/0', 'oJitl0bd590o0ONtSt1nB7hFh0Bo', '', '0', null, null);

-- ----------------------------
-- Table structure for wx_mp_menu
-- ----------------------------
DROP TABLE IF EXISTS `wx_mp_menu`;
CREATE TABLE `wx_mp_menu` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `parentIdx` bigint(20) DEFAULT NULL,
  `menuType` varchar(50) NOT NULL COMMENT '菜单类型：1主菜单，2链接，3文本，4关键字，5扫码，6发图，7发送位置',
  `menuName` varchar(128) NOT NULL,
  `menuKey` varchar(64) DEFAULT NULL,
  `menuUrl` varchar(500) DEFAULT NULL COMMENT '菜单链接',
  `replyContent` varchar(500) DEFAULT NULL,
  `scanType` int(4) DEFAULT NULL COMMENT '扫码类型：1扫码带事件，2扫码带提示',
  `pictureType` int(4) DEFAULT NULL COMMENT '发图类型：1，系统拍照发图；2,，拍照或者相册发图；3，微信相册发图',
  `location` varchar(255) DEFAULT NULL COMMENT '发送位置',
  `sort` int(11) DEFAULT NULL COMMENT '菜单排序',
  `status` int(4) DEFAULT NULL COMMENT '菜单状态',
  `createBy` bigint(20) DEFAULT NULL COMMENT '创建人',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateBy` bigint(20) DEFAULT NULL COMMENT '更新人',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `mpId` bigint(20) NOT NULL COMMENT '微信配置ID',
  `idx` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信菜单表';

-- ----------------------------
-- Records of wx_mp_menu
-- ----------------------------

-- ----------------------------
-- Table structure for wx_mp_wechat_keys
-- ----------------------------
DROP TABLE IF EXISTS `wx_mp_wechat_keys`;
CREATE TABLE `wx_mp_wechat_keys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mpId` bigint(20) NOT NULL COMMENT '公众号id',
  `mediaId` varchar(50) DEFAULT NULL COMMENT '素材Id',
  `keyword` varchar(200) DEFAULT NULL COMMENT '关键字，以#,#隔开',
  `type` tinyint(4) DEFAULT NULL COMMENT '回复类型，1：关键字 2：关注自动回复 3：默认回复',
  `replyType` varchar(20) DEFAULT NULL COMMENT '回复类型，文字：text 图文：news 图片： image 语音：voice 音乐：music 视频：video',
  `content` text COMMENT '内容',
  `newsId` bigint(20) DEFAULT NULL COMMENT '图文素材Id',
  `musicTitle` varchar(100) DEFAULT NULL COMMENT '音乐标题',
  `musicCover` varchar(255) DEFAULT NULL COMMENT '音乐封面',
  `musicUrl` varchar(255) DEFAULT NULL COMMENT '音乐url',
  `musicDesc` varchar(255) DEFAULT NULL COMMENT '音乐描述',
  `imageUrl` varchar(255) DEFAULT NULL COMMENT '图片URL',
  `voiceUrl` varchar(255) DEFAULT NULL COMMENT '音频URL',
  `videoTitle` varchar(100) DEFAULT NULL COMMENT '视频标题',
  `videoUrl` varchar(255) DEFAULT NULL COMMENT '视频url',
  `videoDesc` varchar(255) DEFAULT NULL COMMENT '视频描述',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `updateTime` datetime DEFAULT NULL COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '状态 0：禁用 1：启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信-关键字回复';

-- ----------------------------
-- Records of wx_mp_wechat_keys
-- ----------------------------
